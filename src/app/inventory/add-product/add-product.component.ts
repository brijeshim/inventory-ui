import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent implements OnInit {
  categories = [
    'electronics',
    'clothing',
    'home appliance',
    'beauty products',
    'foot wear',
  ];
  prodctName: any;
  category: any;
  qty: any;
  gender: any;
  size: any;
  constructor() { }

  ngOnInit(): void {
  }

  addItem() {
    
  }
}
