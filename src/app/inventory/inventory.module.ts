import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InventoryRoutingModule } from './inventory-routing.module';
import { InventoryComponent } from './inventory.component';
import { AddProductComponent } from "./add-product/add-product.component";
import { DashboardComponent } from "../dashboard/dashboard.component";
import { AgGridModule } from "ag-grid-angular";
import { ImageCellRendererComponent } from "./image-cell-renderer";
import { ActionCellRendererComponent } from "./action-cell-renderer";
import { ScannerComponent } from './scanner/scanner.component';
import { FormatsDialogComponent } from './scanner/formats-dialog/formats-dialog.component';
import { PermissionsDialogComponent } from './scanner/permissions-dialog/permissions-dialog.component';
import { ZXingScannerModule } from '@zxing/ngx-scanner';
import {OverlayModule} from '@angular/cdk/overlay';
import {A11yModule} from '@angular/cdk/a11y';
import {ClipboardModule} from '@angular/cdk/clipboard';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {PortalModule} from '@angular/cdk/portal';
import {ScrollingModule} from '@angular/cdk/scrolling';
import {CdkStepperModule} from '@angular/cdk/stepper';
import {CdkTableModule} from '@angular/cdk/table';
import {CdkTreeModule} from '@angular/cdk/tree';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatTooltipModule } from '@angular/material/tooltip';
import {MatGridListModule} from '@angular/material/grid-list';

import { MatMenuModule } from '@angular/material/menu';
import { GraphQLModule } from "../graphql.module";
import { MatListModule } from '@angular/material/list';
import { AddBrandDialog, AddCategoryDialog, AddSubCategoryDialog } from "./scanner/scanner.component";
import { AppService } from "../app.service";
@NgModule({
  declarations: [
    InventoryComponent, 
    AddProductComponent,
    ImageCellRendererComponent,
    FormatsDialogComponent,
    PermissionsDialogComponent,
    ActionCellRendererComponent,
    ScannerComponent,
    AddBrandDialog, 
    AddCategoryDialog, 
    AddSubCategoryDialog
],
  imports: [
    CommonModule, 
    InventoryRoutingModule,
    AgGridModule,
    ZXingScannerModule,
    OverlayModule,
    A11yModule,
    ClipboardModule,
    CdkStepperModule,
    CdkTableModule,
    CdkTreeModule,
    DragDropModule,
    PortalModule,
    ScrollingModule,
    FormsModule,
    MatDialogModule,
    MatTooltipModule,
    MatButtonModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatIconModule,
    MatGridListModule,
    MatListModule,
    MatMenuModule,
    GraphQLModule
    ],
  entryComponents: [ImageCellRendererComponent, FormatsDialogComponent, PermissionsDialogComponent, ActionCellRendererComponent],
})
export class InventoryModule {}