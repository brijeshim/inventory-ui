import {Component} from "@angular/core";
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { DomSanitizer, By } from '@angular/platform-browser';

import {ICellRendererParams} from "ag-grid-community";
@Component({
    selector: 'mult-image-renderer',
    template: `
        <div  class="p-1">
            <img *ngIf="this.params.values === 'bar_code'" class="" [src]="this.params.value" [alt]="this.params.values" height="25" width="100"/>
            <img *ngIf="this.params.values === 'qr_code'" class="" [src]="this.params.value" [alt]="this.params.values" height="100" width="100"/>
        </div>
        `
 })
 export class ImageCellRendererComponent implements ICellRendererAngularComp {
    params: any;
    images: any;
    imageSource: any;
    agInit(params: ICellRendererParams) {
        this.params = params;
        this.images = this.params.data[this.params.values];
    }

    refresh(params: ICellRendererParams): boolean {
        this.params = params;
        return true;
    }
    
 }