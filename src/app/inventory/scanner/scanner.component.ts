import { Component, OnInit, Inject } from '@angular/core';
import { BarcodeFormat, DetectorResult } from '@zxing/library';
import { BehaviorSubject } from 'rxjs';
import { FormatsDialogComponent } from './formats-dialog/formats-dialog.component';
import { PermissionsDialogComponent } from './permissions-dialog/permissions-dialog.component';
import { gql, Apollo, WatchQueryOptions } from "apollo-angular";
import { Observable, Subscription } from 'rxjs';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { AppService } from "../../app.service";
import {MatSnackBar} from '@angular/material/snack-bar';


@Component({
  selector: 'add-brand-dialog',
  templateUrl: 'add-brand-dialog.html',
})
export class AddBrandDialog {
  brand: any;
  description: any;
  constructor(
    public dialogRef: MatDialogRef<AddBrandDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    console.log("branddata",data)
  }
}

@Component({
  selector: 'add-category-dialog',
  templateUrl: 'add-category-dialog.html',
})
export class AddCategoryDialog {
  category: any;
  description: any;
  constructor(
    public dialogRef: MatDialogRef<AddCategoryDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {}
}

@Component({
  selector: 'add-subcategory-dialog',
  templateUrl: 'add-subcategory-dialog.html',
})
export class AddSubCategoryDialog implements OnInit {
  subcategory: any;
  description: any;
  constructor(
    public dialogRef: MatDialogRef<AddSubCategoryDialog>,
    @Inject(MAT_DIALOG_DATA) public data: {
      categoryId: string, 
      categoryName: string, 
      description: string}
  ) {
  }
  ngOnInit() {

    console.log("cat ddata",this.data)
  }
  // closeDialog() {
  //   this.dialogRef.close({'categotyId':this.data.categoryId, 'subCategoryName':this.subcategory,'description':this.description});
  // }
}
const ProductInfoDataQuery = gql`
{
  inventory_brand {
    brand_name
    brand_id
    description
  }
  inventory_category {
    category_name
    category_id
    description
  }
  inventory_sub_category {
    sub_category_id
    category_id
    sub_category_name
  }
  inventory_order_items {
    create_date
    created_by
    discount
    order_id
    price
    product_id
    quantity
    update_date
    updated_by
  }
}`;

const AddProductQuery = gql`mutation AddProductQuery(
  $basePrice: numeric!,
  $brandId: String!,
  $categoryId: String!,
  $createDate: String!,
  $createdBy: String!,
  $mfgDate: String!,
  $productCode: String!,
  $productId: String!,
  $productName: String!,
  $sellingPrice: numeric!,
) {
  insert_inventory_product(objects: {
    base_price: $basePrice,
    brand_id: $brandId,
    category_id: $categoryId,
    create_date: $createDate,
    created_by: $createdBy,
    description: "",
    is_active: true,
    manufacturing_date: $mfgDate,
    product_code: $productCode,
    product_id: $productId,
    product_name: $productName,
    sellig_price: $sellingPrice,
    updated_by: $createdBy,
    updated_date: $createDate
  }) {
    returning {
      base_price
      brand_id
      category_id
      create_date
      created_by
      description
      is_active
      manufacturing_date
      product_code
      product_id
      product_name
      sellig_price
      updated_by
      updated_date
    }
  }
}
`;

@Component({
  selector: 'app-scanner',
  templateUrl: './scanner.component.html',
  styleUrls: ['./scanner.component.scss']
})
export class ScannerComponent implements OnInit {

  availableDevices: any;
  currentDevice:any;

  formatsEnabled: BarcodeFormat[] = [
    BarcodeFormat.CODE_128,
    BarcodeFormat.DATA_MATRIX,
    BarcodeFormat.EAN_13,
    BarcodeFormat.QR_CODE,
  ];

  hasDevices: boolean =false;
  hasPermission: boolean = false;

  qrResultString: any;

  torchEnabled = false;
  torchAvailable$ = new BehaviorSubject<boolean>(false);
  tryHarder = false;
  categories = [
    'electronics',
    'clothing',
    'home appliance',
    'beauty products',
    'foot wear',
    'medical',
  ];
  subCategory : { [key: string]: any } = {
    'electronics': [
      {
        'sub-category':'mobile', 
        'brands':['apple','samsung','nokia','mi','oppo']
      }, 
      {
        'sub-category':'laptop',
        'brands':['apple','samsung','lenovo','dell','hp']
      }, 
      {
        'sub-category':'smart watch',
        'brands':['apple','samsung','lenovo','dell','hp']
      },  
      {
        'sub-category': 'headphone',
        'brands':['apple','samsung','jbl','bose','sony']
      },   
      {
        'sub-category': 'camera',
        'brands':['samsung','nikon','cannon','sony']
      },   
      {
        'sub-category': 'speakers',
        'brands':['apple','samsung','jbl','bose','sony']
      },  
      {
        'sub-category':'tablet',
        'brands':['apple','samsung','lenovo','dell','hp']
      },    
      {
        'sub-category':'networking devices',
        'brands':['cisco','samsung','lenovo','dell','hp']
      }],
    'clothing': {
      'male': ['t-Shirt', 'casual shirt', 'formal shirt', 'denims', 'watches','trouser', 'winter wear', 'inner wear', 'jacket', 'suit', 'jeans'],
      'male-child': ['t-Shirt', 'casual shirt', 'formal shirt', 'denims', 'watches','trouser', 'winter wear', 'inner wear', 'jacket', 'suit', 'jeans'],
      'female': ['t-Shirt', 'casual shirt', 'formal shirt', 'denims', 'watches','trouser', 'winter wear', 'inner wear', 'jacket', 'suit', 'jeans'],
      'female-child': ['t-Shirt', 'casual shirt', 'formal shirt', 'denims', 'watches','trouser', 'winter wear', 'inner wear', 'jacket', 'suit', 'jeans'],
    },
    'home appliance': ['tv', 'air conditioner', 'cooler', 'fan', 'gas stove', 'microwave', 'refregerator', 'washing machine', 'dish washer'],
    'beauty products': ['fairness cream', 'face wash', 'perfume'],
    'foot wear': ['shoes', 'sandles', 'slipers', 'sports shoes', 'casual shoes', 'formal shoes'],
    'medical': ['heart rate monitor', 'oxygen meter']
  }
  prodctName: any;
  category: any;
  categorySelected: any;
  qty: any;
  gender: any;
  size: any;
  subCategorySelected: any;
  brandSelected: any;
  basePrice: any;
  sellingPrice: any;
  productCode: any;
  mfgDate: any;
  allowedImageFormats: string[] = ['png', 'jpeg', 'jpg']
  private querySubscription: any;
  private brandData: any;
  private categoryData: any;
  private subCategoryData: any;
  public brandItems: any;
  public categoryItems: any;
  public subCategoryItems: any;
  public subCategoryShow: any;
  public multipleFiles: any[] = [];
  constructor(
    private _snackBar: MatSnackBar,
    private appService: AppService,private readonly _dialog: MatDialog,
    private apollo: Apollo) { }
  
  ngOnInit() {
    //this.getData();
    this.getCatgory();
    this.getSubCatgory();
    this.getBrands();
    this.getAllFeatures()
  }

  getData() {
    this.querySubscription = this.apollo.watchQuery<any>({query: ProductInfoDataQuery}).valueChanges.subscribe((result: any) => {
      if(result['data']) {
        this.brandData = result['data']["inventory_brand"];
        this.categoryData = result['data']["inventory_category"];
        this.subCategoryData = result['data']["inventory_sub_category"];
        this.setupDropdownItems(this.brandData, this.categoryData, this.subCategoryData);
      }
      console.log("---------",result,this.brandData,this.categoryData,this.subCategoryData)
    })
  }

  setupDropdownItems(brandData: any, categoryData: any, subCategoryData: any) {
    this.brandItems = brandData.map((item: any) => {
      return {
        brandId: item.brandId,
        brandName: item.brandName
      }
    })
    this.categoryItems = categoryData.map((item: any) => {
      return {
        categoryId: item.categoryId,
        categoryName: item.categoryName
      }
    });
    this.category = this.categoryItems[0].categoryId;
    this.subCategoryItems = subCategoryData.map((item: any) => {
      return {
        subCategoryId: item.subCategoryId,
        subCategoryName: item.subCategoryName,
        categoryId: item.categoryId
      }
    });
  }

  onChangeCategory() {
    console.log(this.category)
    this.categorySelected = this.categoryItems.filter((item: any) => {
      return item.categoryId === this.category;
    })[0];

    this.getSubCatgory();
  }

  addProductApollo() {
    const date = (new Date).getTime();
    this.apollo.mutate({
      mutation: AddProductQuery,
      variables: {
        "basePrice": this.basePrice,
        "brandId": this.brandSelected,
        "categoryId": this.category,
        "createDate": date.toString(),
        "createdBy": "1",
        "mfgDate": date.toString(),
        "productCode": "tyututyu",
        "productId": "efwwer",
        "productName": this.prodctName,
        "sellingPrice": this.sellingPrice
      }
    }).subscribe((result: any) => {
        console.log("add result", result)
    },(error) => {
        console.log('there was an error sending the query', error);
    });
  }

  addProduct() {
    const date = (new Date).getTime();
    const data = {
      "basePrice": this.basePrice,
      "brandId": this.brandSelected,
      "categoryId": this.category,
      "description": "string",
      "manufacturedDate": this.mfgDate,
      "productCode": this.qrResultString,
      "productName": this.prodctName,
      "quantity": this.qty,
      "sellingPrice": this.sellingPrice,
      "subCategoryId": this.subCategorySelected
      //"supplierId": "string"
    }
    this.appService.addProduct(data).subscribe((result: any) => {
      this._snackBar.open(`Product added successfully`,'Dismiss', {
        duration: 3000
      });
    },(error) => {
        console.log('there was an error sending the query', error);
    });
  }

  clearResult(): void {
    this.qrResultString = null;
  }

  onCamerasFound(devices: MediaDeviceInfo[]): void {
    this.availableDevices = devices;
    this.hasDevices = Boolean(devices && devices.length);
  }

  onCodeResult(resultString: any) {
    this.qrResultString = resultString;
  }

  onDeviceSelectChange(event: any) {
    const selected = this.getValue(event)
    const device = this.availableDevices.find((x: any) => x.deviceId === selected);
    this.currentDevice = device || null;
  }

  openFormatsDialog() {
    const data = {
      formatsEnabled: this.formatsEnabled,
    };

    this._dialog
      .open(FormatsDialogComponent, { data })
      .afterClosed()
      .subscribe(x => { if (x) { this.formatsEnabled = x; } });
  }

  onHasPermission(has: boolean) {
    this.hasPermission = has;
  }

  openInfoDialog() {
    const data = {
      hasDevices: this.hasDevices,
      hasPermission: this.hasPermission,
    };

    this._dialog.open(PermissionsDialogComponent, { data });
  }

  onTorchCompatible(isCompatible: boolean): void {
    this.torchAvailable$.next(isCompatible || false);
  }

  toggleTorch(): void {
    this.torchEnabled = !this.torchEnabled;
  }

  toggleTryHarder(): void {
    this.tryHarder = !this.tryHarder;
  }
  getValue(event: Event): string {
    return (event.target as HTMLInputElement).value;
  }

  getSubCategoryItems(category: any) {
    let items: string[] = [];
    this.subCategory[category].forEach((element:any) => {
      items.push(element['sub-category'])
    });
    return items;
  }
  getBrandItems(category: any, subcate: any) {
    return this.subCategory[category].filter((item: any) => {
        return item['sub-category'] === subcate;
    })[0]['brands'];
  }

  addItem() {
    console.log("added")
  }
  onFileSelect(event: any) {
    const files = event.target.files;
    console.log(files)
    for(let i=0;i<files.length;i++) {
      const reader = new FileReader();
      const filename = files[i].name;
      const type = files[i].type;
      reader.onload =this._handleReaderLoaded.bind(filename, type, this);
      reader.readAsBinaryString(files[i]);
    }
    // const file = files[0];

    // if (file) {
    //   const reader = new FileReader();
    //   reader.onload =this._handleReaderLoaded.bind(this);
    //   reader.readAsBinaryString(file);
    // }
  }
  _handleReaderLoaded(filename: any, type: any,readerEvt: any) {
    const binaryString = readerEvt.target.result;
    const base64string = btoa(binaryString);
    const file = {
      encodedFile: base64string,
      contentType: type,
      fileName: filename
    };
    this.multipleFiles.push([file])
    //this.base64textString= btoa(binaryString);
    console.log(" this.multipleFiles", this.multipleFiles);
    // return btoa(binaryString);
  }

  openAddBrandDialog() {
    const dialogRef = this._dialog.open(AddBrandDialog, { disableClose: true, data: this.category });
    dialogRef.afterClosed().subscribe((result: any) => {
      console.log(`Dialog result: ${result}`);
      if(result){
        this.appService.addBrand(result).subscribe((response: any) => {
          this.getBrands();
          this._snackBar.open(`Brand ${response.brandName}
          added successfully.`,'Dismiss', {
            duration: 3000
            });
        }, (error) => {
          console.log(error)
        })
      } else {
        this._snackBar.open(`Please provide required data`,'Dismiss', {
            duration: 3000
            });
      }
      
    });
	}

  openAddCategoryDialog() {
    const dialogRef = this._dialog.open(AddCategoryDialog, { disableClose: true });
    dialogRef.afterClosed().subscribe((result: any) => {
      console.log(`Dialog result: ${result}`);
      if(result){
      this.appService.addCategory(result).subscribe((response: any) => {
        this.getCatgory();
        this._snackBar.open(`Category ${response.categoryName} added successfully.`,'Dismiss', {
          duration: 3000
          });
        
      }, (error) => {
        console.log(error)
      })
    } else {
      this._snackBar.open(`Please provide required data`,'Dismiss', {
          duration: 3000
          });
    }
    });
	}

  openAddSubCategoryDialog() {
    console.log("category selected",this.categorySelected)
    const dialogRef = this._dialog.open(AddSubCategoryDialog, { data:  this.categorySelected});
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      if(result && result.categoryId && result.subCategoryName){
      this.appService.addSubCategory(result).subscribe((response: any) => {
        this._snackBar.open(`Sub-Category ${response.subCategoryName} added successfully.`,'Dismiss', {
          duration: 3000
          });
      }, (error) => {
        console.log(error)
      })
      this.getSubCatgory();
    } else {
      this._snackBar.open(`Please provide required data`,'Dismiss', {
          duration: 3000
        });
    }
    });
	}

  getCatgory() {
    this.appService.getCategory().subscribe((response: any) => {
     this.categoryItems = response.map((item: any) => {
       return {
        categoryId: item.categoryId,
        categoryName: item.categoryName,
        description: item.description
       }
     })
     this.onChangeCategory()
    }, (error) => {
      console.log(error)
    })
  }

  getSubCatgory() {
    this.appService.getSubCategory(this.category).subscribe((response: any) => {
     this.subCategoryItems = response.map((item: any) => {
       return {
        subCategoryId: item.subCategoryId,
        subCategoryName: item.subCategoryName,
        description: item.description
       }
     })
     console.log("subCategoryItems",this.subCategoryItems)
    }, (error) => {
      console.log(error)
    })
  }

  getBrands() {
    this.appService.getBrands().subscribe((response: any) => {
     this.brandItems = response.map((item: any) => {
       return {
        brandId: item.brandId,
        brandName: item.brandName,
        description: item.description
       }
     })
    }, (error) => {
      console.log(error)
    })
  }

  getAllFeatures() {
    this.appService.getFeatures().subscribe((result: any) => {
      console.log(result)
    },( error: any )=>{
      console.log(error);
    })
  }

  
  resetInputs() {
    this.prodctName = undefined;
    this.category = undefined;
    this.qty = undefined;
    this.gender = undefined;
    this.size = undefined;
    this.subCategorySelected = undefined;
    this.brandSelected = undefined;
  }
}

