import { Component, OnInit, OnDestroy } from '@angular/core';
import { ImageCellRendererComponent } from "./image-cell-renderer";
import {MatDialog} from '@angular/material/dialog';
import { AddProductComponent } from "./add-product/add-product.component";
import { GridOptions } from "ag-grid-community";
import { ActionCellRendererComponent } from "./action-cell-renderer";
import { AppService } from "../app.service";
import { gql, Apollo, WatchQueryOptions } from "apollo-angular";
import {MatSnackBar} from '@angular/material/snack-bar';
import { Observable, Subscription } from 'rxjs';
import { DomSanitizer } from '@angular/platform-browser'

const GetAllProductsQuery = gql`query MyQuery($origin: String) {
	inventory_product(where: {origin: {_eq: $origin}}) {
	  bar_code
	  base_price
	  brand_id
	  category_id
	  create_date
	  created_by
	  description
	  expiry_date
	  is_active
	  manufacturing_date
	  origin
	  product_code
	  product_name
	  product_id
	  qr_code
	  quantity
	  sellig_price
	  sub_category_id
	  updated_by
	  updated_date
	}
  }`;

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.scss']
})
export class InventoryComponent implements OnInit, OnDestroy {
	public gridApi: any;
	public gridColumnApi: any;
	public frameworkComponents: any;
	public columnDefs: any;
	public rowData: any;
	public searchText: any;
	public gridOptions: any;
	public rowSelection: any;
	public defaultColDef: any;
	public perPage = 10;
	public userData: any;
	editType: any;
	private querySubscription: Subscription = new Subscription();
  constructor(public dialog: MatDialog, private appService: AppService, 
	private _snackBar: MatSnackBar,
	private sanitizer: DomSanitizer,
	private apollo: Apollo) {
		this.frameworkComponents = {
			multiImageRendered: ImageCellRendererComponent,
			actionCellRenderer: ActionCellRendererComponent
		}
		this.defaultColDef = {
			sortable: true,
			resizable: true,
			filter: true,
			flex: 1,
			minWidth: 100,
			suppressDragLeaveHidesColumns: true,
			suppressNavigable: true,
			};
    	this.rowSelection = 'multiple';
		this.gridOptions = {
			context: this
		}
		this.editType = 'fullRow';
	}

  	ngOnInit() {
		const attr = localStorage.getItem('user');
		const userAttr = JSON.parse(JSON.parse(JSON.stringify(attr)));
		console.log('userGroups', userAttr);
		this.userData = userAttr;
		this.getAllProductsApollo()
	}
  	onGridReady(params: any) {
		this.gridApi = params.api;
		this.gridColumnApi = params.columnApi;
		this.columnDefs = [
			//{headerName: 'S.No.', width: 80,  valueGetter: "node.rowIndex + 1", pinned: 'left', lockPosition: true },
			{headerName: 'Product Id',width: 220, field: 'product_id', pinned: "left",filter: true, lockPosition: true,getQuickFilterText: function(params: any) {
				return params.value;
			}},
			{headerName: 'Product Code',width: 220, field: 'product_code', pinned: "left",filter: true, lockPosition: true,getQuickFilterText: function(params: any) {
				return params.value;
			}},
			{headerName: 'Product Name',width: 220, editable: true, field: 'product_name', pinned: "left",filter: true, lockPosition: true,getQuickFilterText: function(params: any) {
				return params.value;
			}},
			{headerName: 'QR-Code',width: 120,  field: "qr_code", cellRenderer: 'multiImageRendered' ,cellRendererParams: {values: 'qr_code'}},
			{headerName: 'BAR-Code',width: 120,  field: "bar_code", cellRenderer: 'multiImageRendered', cellRendererParams: {values: "bar_code"}},
			{headerName: 'Quantity',width: 120, field: 'quantity', editable: true},
			{headerName: 'Base Price',width: 120, field: 'base_price', editable: true},
			{headerName: 'Selling Price',width: 220, field: 'sellig_price', editable: true},
			{headerName: 'Expiry Date',width: 220, field: 'expiry_date'},
			{headerName: 'Is Active',width: 120, field: 'is_active', pinned: 'right', editable: true},
			{headerName: 'Action',width: 120, cellRenderer: 'actionCellRenderer', pinned: 'right', lockPosition: true}
		];

		
	
		// this.rowData = [
		// 	{ product_id:1, make: 'Toyota', model: 'Celica', price: 35000, qty: 10, images: ['/assets/images/product.png'], desc: "", specification: [] },
		// 	{ product_id:2,make: 'Ford', model: 'Mondeo', price: 32000, qty: 10, images: ['/assets/images/product.png'], desc: "", specification: []  },
		// 	{ product_id:3,make: 'Porsche', model: 'Boxter', price: 72000, qty: 10,images: ['/assets/images/product.png'], desc: "", specification: []  },
		// 	{ product_id:1, make: 'Toyota', model: 'Celica', price: 35000, qty: 10, images: ['/assets/images/product.png'], desc: "", specification: []  },
		// 	{ product_id:2,make: 'Ford', model: 'Mondeo', price: 32000, qty: 10, images: ['/assets/images/product.png'], desc: "", specification: []  },
		// 	{ product_id:3,make: 'Porsche', model: 'Boxter', price: 72000, qty: 10,images: ['/assets/images/product.png'], desc: "", specification: []  },
		// 	{ product_id:1, make: 'Toyota', model: 'Celica', price: 35000, qty: 10, images: ['/assets/images/product.png'], desc: "", specification: []  },
		// 	{ product_id:2,make: 'Ford', model: 'Mondeo', price: 32000, qty: 10, images: ['/assets/images/product.png'], desc: "", specification: []  },
		// 	{ product_id:3,make: 'Porsche', model: 'Boxter', price: 72000, qty: 10,images: ['/assets/images/product.png'], desc: "", specification: []  },
		// 	{ product_id:1, make: 'Toyota', model: 'Celica', price: 35000, qty: 10, images: ['/assets/images/product.png'], desc: "", specification: []  },
		// 	{ product_id:2,make: 'Ford', model: 'Mondeo', price: 32000, qty: 10, images: ['/assets/images/product.png'], desc: "", specification: []  },
		// 	{ product_id:3,make: 'Porsche', model: 'Boxter', price: 72000, qty: 10,images: ['/assets/images/product.png'], desc: "", specification: []  }
		// ];
		
		this.gridApi.sizeColumnsToFit()
	}

	openDialog() {
    const dialogRef = this.dialog.open(AddProductComponent, { disableClose: true });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
	}
	
	onFilterTextBoxChanged() {
    this.gridApi.setQuickFilter(this.searchText);
	}

	getData() {
		this.appService.getAllProducts().subscribe((result: any) => {
			this.rowData = result.map((item: any) => {
				return {
					"bar_code": this.sanitizer.bypassSecurityTrustResourceUrl(`data:image/jpg;base64, ${item.bar_code}`),
					"base_price": item.base_price,
					"brand_id": item.brand_id,
					"category_id": item.category_id,
					"create_date": item.create_date,
					"created_by": item.created_by,
					"description": item.description,
					"expiry_date": item.expiry_date,
					"is_active":item.is_active,
					"manufacturing_date": item.manufacturing_date,
					"origin": item.origin,
					"product_code": item.product_code,
					"product_name": item.product_name,
					"qr_code": this.sanitizer.bypassSecurityTrustResourceUrl(`data:image/jpg;base64, ${item.qr_code}`),
					"quantity": item.quantity,
					"sellig_price": item.sellig_price,
					"sub_category_id":item.sub_category_id,
					"updated_by": item.updated_by,
					"updated_date": item.updated_date
				}
			})
		})
	}

	getAllProductsApollo() {
		this.querySubscription = this.apollo.watchQuery<any>({
		  query: GetAllProductsQuery,
		  variables: {
			  origin: this.userData['x-origin']
		  },
		  pollInterval: 5000
		}).valueChanges.subscribe((result: any) => {
			this.rowData = result['data']['inventory_product'].map((item: any) => {
				return {
					"bar_code": this.sanitizer.bypassSecurityTrustResourceUrl(`data:image/jpg;base64, ${item.bar_code}`),
					"base_price": item.base_price,
					"brand_id": item.brand_id,
					"category_id": item.category_id,
					"create_date": item.create_date,
					"created_by": item.created_by,
					"description": item.description,
					"expiry_date": item.expiry_date,
					"is_active":item.is_active,
					"manufacturing_date": item.manufacturing_date,
					"origin": item.origin,
					"product_code": item.product_code,
					"product_name": item.product_name,
					"product_id": item.product_id,
					"qr_code": this.sanitizer.bypassSecurityTrustResourceUrl(`data:image/jpg;base64, ${item.qr_code}`),
					"quantity": item.quantity,
					"sellig_price": item.sellig_price,
					"sub_category_id":item.sub_category_id,
					"updated_by": item.updated_by,
					"updated_date": item.updated_date
				}
			});
		},(error) => {
			console.log('there was an error sending the query', error);
		});
	}

	ngOnDestroy() {
		this.querySubscription.unsubscribe()
	}
}
