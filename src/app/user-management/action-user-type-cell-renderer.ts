import {Component, AfterViewInit } from "@angular/core";
import { ICellRendererAngularComp, AgEditorComponent } from 'ag-grid-angular';

import {ICellEditorParams} from "ag-grid-community";
@Component({
    selector: 'action-cell-renderer',
    template: `
    <div style="display:felx; flex-direction: row; width:100%;">
    <ng-container *ngIf="type === 'group'">
        <select [(ngModel)]="params.value" (change)="onClick(true)">
            <option *ngFor="let group of groupData" [value]="group.roleId">{{group.roleName}}</option>
        </select>
    </ng-container>
    </div>`
 })
 export class ActionDropdownUserTypeCellRendererComponent implements AfterViewInit , AgEditorComponent {
    params: any;
    type: any;
    edit: boolean = false;
    value: any;
    oldValue: any;
    groupData: any;
    agInit(params: ICellEditorParams) {
        this.params = params;
    }

    refresh(params: ICellEditorParams): boolean {
        this.params = params;
        return true;
    }
    isPopup(): boolean {
        return true;
      }
    setSelected(selected: any): void {

    this.value = selected;
    }

    setRole() {
        this.params.context.componentParent.setRole(this.value)
    }
    getValue(): any {
        return this.value;
    }
    ngAfterViewInit() {
    }
    
 }