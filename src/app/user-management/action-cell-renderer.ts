import {Component} from "@angular/core";
import { ICellRendererAngularComp } from 'ag-grid-angular';

import {ICellRendererParams} from "ag-grid-community";
@Component({
    selector: 'action-cell-renderer',
    template: `
    <div style="display:felx; flex-direction: row;">
        <button mat-icon-button [disabled]="disableEdit()" style="cursor:pointer;" (click)="edit = !edit; editClicked();" *ngIf="!edit"><mat-icon>edit</mat-icon></button>
        <a style="cursor:pointer;" (click)="edit = !edit;stopEditing()" *ngIf="edit"><mat-icon style="color: green;">check</mat-icon></a>
        <a style="cursor:pointer;" (click)="edit = !edit;stopEditing()" *ngIf="edit"><mat-icon style="color: black;">close</mat-icon></a>
    </div>`
 })
 export class ActionCellRendererComponent implements ICellRendererAngularComp {
    params: any;
    edit: boolean = false;
    userData: any;
    agInit(params: ICellRendererParams) {
        this.params = params;
        console.log("params",params)
    }

    refresh(params: ICellRendererParams): boolean {
        this.params = params;
        return true;
    }

    editClicked(){
        this.params.api.setFocusedCell(this.params.rowIndex, 'roles');
        this.params.api.startEditingCell({
            rowIndex: this.params.rowIndex,
            colKey: 'roles',
          });
    }
    stopEditing() {
        this.params.api.stopEditing();
    }

    disableEdit() {
        return this.params.user['sub'] === this.params.data['userId']? true: false;
    }
    
 }