import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserManagementComponent } from './user-management.component';
import { AddUserComponent } from "./add-user/add-user.component";
import { UserListComponent } from "./user-list/user-list.component";

const routes: Routes = [
  { 
    path: '', 
    component: UserManagementComponent,
    children: [
      {
        path: '',
        pathMatch: 'prefix',
        redirectTo: 'user-list'
      },
      {
        path: 'add-user',
        component: AddUserComponent
      },
      {
        path: 'user-list',
        component: UserListComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserManagementRoutingModule { }
