import { Component, OnInit } from '@angular/core';
import { AppService } from "../../app.service";
import { ActionCellRendererComponent } from "../action-cell-renderer";
import { ActionDropdownCellRendererComponent } from "../action-activate-cell-renderer";
import { GridOptions } from "ag-grid-community";
import {MatSnackBar} from '@angular/material/snack-bar';
@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
  	public gridApi: any;
	public gridColumnApi: any;
	public frameworkComponents: any;
	public columnDefs: any;
	public rowData: any;
	public searchText: any;
	public gridOptions: any;
	public rowSelection: any;
	public defaultColDef: any;
	public discount: any;
	public perPage = 10;
	userData: any;
	roles: any;
	editType: any;
  constructor(private appService: AppService, private _snackBar: MatSnackBar) {
    this.frameworkComponents = {
			actionCellRenderer: ActionCellRendererComponent,
      		dropdownCellRenderer: ActionDropdownCellRendererComponent
		}
		this.gridOptions = <GridOptions>{
			context: {
				componentParent: this
			}
		};
    this.defaultColDef = {
			sortable: true,
			resizable: true,
			filter: true,
			flex: 1,
			minWidth: 100,
      suppressDragLeaveHidesColumns: true,
			};
    	this.rowSelection = 'multiple';
		this.editType = 'fullRow';
   }

  ngOnInit(): void {
	const attr = localStorage.getItem('user');
	this.userData = JSON.parse(JSON.parse(JSON.stringify(attr)));
	
    
  }
  onGridReady(params: any) {
		this.gridApi = params.api;
		this.gridColumnApi = params.columnApi;
		this.getAllROles();
		
    this.getAllUsers();
	}

  getAllUsers() {
    let users: any;
    this.appService.getAllUsers().subscribe((result: any) => {
      this.gridApi.setRowData(result);
      this.rowData = result.map((item: any) => {
		  return {
			active: item.active,
			email: item.email,
			firstName: item.firstName,
			lastName: item.lastName,
			roles: item.roles ? item.roles[0] : '',
			userId: item.userId,
			userName: item.userName,
		  }
	  });
    })
  }


  getAllROles() {
	  this.appService.getRoles().subscribe((result: any) => {
		  this.roles = result;
		  const toRoles = ['None',...result.map((item: any) => {
			  return item.roleName;
		  })];
		  console.log('toroles',toRoles)
		  this.columnDefs = [
			{headerName: 'S.No.', width: 80,  valueGetter: "node.rowIndex + 1", pinned: 'left' },
			{headerName: 'Username',filter: true,pinned: 'left', field: 'userName',getQuickFilterText: function(params: any) {
				return params.value;
			}},
			{headerName: 'First Name', field: 'firstName', pinned: 'left',filter: true,getQuickFilterText: function(params: any) {
				return params.value;
			}},
			{headerName: 'Last Name', pinned: 'left',  field: "lastName", params: "images" },
			{headerName: 'Email', field: 'email'},
      		{headerName: 'User Type', field: 'roles', editable: true, cellEditor: 'agSelectCellEditor',cellEditorParams: {values: toRoles}},
			{headerName: 'Active', field: 'active', editable: true, cellEditor: 'agSelectCellEditor',cellEditorParams: {values: [true,false]}},
      		{headerName: 'Action', cellRenderer: 'actionCellRenderer', pinned: 'right', cellRendererParams: {
				user: this.userData
			  }
			}
		];
		console.log("result", result)
	},(error) => {
		console.log(error)
	})
  }

  	onFilterTextBoxChanged() {
    	this.gridApi.setQuickFilter(this.searchText);
	}

	onCellValueChanged(params: any) {
		const colId = params.column.getId();
		console.log("params",params, colId)
		if (colId === "active") {
			this.setActive(params.newValue);
		} else if(colId === "roles") {
			this.setRole(params.newValue);
		}
	}

	setActive(val: boolean) {
		this.appService.setActive(this.userData['sub'],val).subscribe((result: any) => {
			this._snackBar.open(`User active status set to ${val}`,'Dismiss', {
				duration: 3000
			  });
		},(error) => {
			console.log(error)
		})
	}

	
	setRole(role: string) {
		const roleId = this.roles.filter((item : any) => {
			return item.roleName === role;
		})[0]['roleId'];
		this.appService.assignRole(this.userData['sub'],roleId).subscribe((result: any) => {
		  this._snackBar.open(`User role set to ${role}`,'Dismiss', {
			duration: 3000
		  });
		},(error) => {
			console.log(error)
		})
	}

	
}
