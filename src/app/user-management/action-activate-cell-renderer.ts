import {Component, OnInit} from "@angular/core";
import { ICellRendererAngularComp, AgEditorComponent } from 'ag-grid-angular';

import {ICellEditorParams} from "ag-grid-community";
@Component({
    selector: 'action-cell-renderer',
    template: `
    <div style="display:felx; flex-direction: row; width:100%;">
    <ng-container *ngIf="type === 'group'">
        <select [(ngModel)]="params.value" (change)="onClick(true)">
            <option *ngFor="let group of groupData" [value]="group.roleId">{{group.roleName}}</option>
        </select>
    </ng-container>
    <ng-container *ngIf="type === 'active'">
        <select [(ngModel)]="params.value" (click)="onClick(true)">
            <option value="true">Active</option>
            <option value="false">Inactive</option>
        </select>
    </ng-container>
    </div>`
 })
 export class ActionDropdownCellRendererComponent implements OnInit, AgEditorComponent {
    params: any;
    type: any;
    edit: boolean = false;
    value: any;
    oldValue: any;
    groupData: any;
    groupSelected: any;
    activeSelected: any;
    agInit(params: ICellEditorParams) {
        this.params = params;
    }

    refresh(params: ICellEditorParams): boolean {
        this.params = params;
        return true;
    }
    isPopup(): boolean {
        return true;
      }
    setSelected(selected: any): void {

    this.value = selected;
    }
    onClick(selected: boolean) {
        if(this.type === 'group'){
            //this.setRole()
            // const filtered = this.groupData.filter((item: any) => {
            //     return selected === item.roleId;
            // })
            // this.value = filtered[0].roleId;
        } else {
            if(this.params.value === 'true') {
                this.setSelected('active')
                this.activeSelected = 'active';
            } else {
                this.activeSelected = 'inactive';
                this.setSelected(this.params.value)
            }
            
        }
    this.params.api.stopEditing();
    }

    setRole() {
        this.params.context.componentParent.setRole(this.value)
    }
    getValue(): any {
        if (this.value === '') {
          this.value = this.oldValue;
        }
        return this.value;
    }
    ngOnInit() {
        this.type = this.params.type;
        if(this.type === 'group'){
            this.groupData = this.params.groupData;
            this.setSelected(this.params.value)
            console.log("this.params.value",this.params.value)
        } else {
            if(this.params.value === 'true') {
                this.setSelected('active')
                this.activeSelected = 'active';
            } else {
                this.activeSelected = 'inactive';
                this.setSelected(this.params.value)
            }
            
        }
      }
    
 }