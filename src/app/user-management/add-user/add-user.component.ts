import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppService } from "../../app.service";
@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {
  loading: any;
  addUserForm: any;
  submitted: boolean = false;
  groups: any;
  roles: any;
  userData: any;
  genPass: boolean = false;
  constructor(private formBuilder: FormBuilder,private appService: AppService) {
    this.addUserForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.required],
      role: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(8)]]
  });
   }

  ngOnInit(): void {
    this.getRoles();
    //this.getGroups();
    const attr = localStorage.getItem('user');
    const userAttr = JSON.parse(JSON.parse(JSON.stringify(attr)));
    this.userData = userAttr;
  }

  getGroups() {
    this.loading = true;
    const attr = localStorage.getItem('user');
    const userAttr = JSON.parse(JSON.parse(JSON.stringify(attr)));
    this.userData = userAttr;
    console.log('userGroups', userAttr);
    const org = userAttr['user-group'][0].split('-')[0].replace(/\//g,'');
    console.log("org", org)
    this.appService.getAllGroups().subscribe((result: any) => {
      this.groups = result.filter((item: any) => {
        return item.indexOf(org) !== -1;
      });
      this.loading = false;
    },( error )=>{
      console.log(error);
      this.loading = false;
    })
  }

  onSubmit() {
    this.loading = true;
    const data = {
      "email": this.addUserForm.controls['email'].value,
      "firstName": this.addUserForm.controls['firstName'].value,
      "roleId": this.addUserForm.controls['role'].value,
      "lastName": this.addUserForm.controls['lastName'].value,
      "password": this.addUserForm.controls['password'].value,
      'origin': this.userData['x-origin']
    }
    console.log("form data",data)
    this.submitted = true;
    this.appService.createUser(data).subscribe((result: any) => {
      alert(result);
      this.addUserForm.reset();
      this.loading = false;
    },( error )=>{
      console.log(error);
      this.loading = false;
    })
  }
  get f() { return this.addUserForm.controls; }

  createUser() {
    
    // this.appService.createUser(data).subscribe((result: any) => {
    //   this.groups = result;
    // },( error )=>{
    //   console.log(error)
    // })
  }

  generatePassword() {
    const temp = btoa('helloworld').slice(0,8);
    this.addUserForm.controls['password'].patchValue(temp);
  }

  getRoles() {
    this.appService.getRoles().subscribe((result: any) => {
      this.roles = result.filter((item: any) => {
        return item.roleName !== 'org-admin'
      });
    },( error )=>{
      console.log(error)
    })
  }

  resetInput() {
    this.addUserForm.controls['email'].patchValue('');
    this.addUserForm.controls['firstName'].patchValue('');
    this.addUserForm.controls['lastName'].patchValue('');
    this.addUserForm.controls['role'].patchValue('');
    this.addUserForm.controls['password'].patchValue('');
  }

}
