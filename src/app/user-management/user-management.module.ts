import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserManagementRoutingModule } from './user-management-routing.module';
import { UserManagementComponent } from './user-management.component';
import { UserListComponent } from './user-list/user-list.component';
import { AddUserComponent } from './add-user/add-user.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AgGridModule } from "ag-grid-angular";
import { ActionCellRendererComponent } from "./action-cell-renderer";
import { ActionDropdownCellRendererComponent } from "./action-activate-cell-renderer";
import { MatIconModule } from '@angular/material/icon';
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { MatButtonModule } from '@angular/material/button';
@NgModule({
  declarations: [
    UserManagementComponent,
    UserListComponent,
    AddUserComponent,
    ActionCellRendererComponent,
    ActionDropdownCellRendererComponent
  ],
  imports: [
    CommonModule,
    UserManagementRoutingModule,
    FormsModule, 
    ReactiveFormsModule,
    MatIconModule,
    AgGridModule.withComponents([]),
    MatSnackBarModule,
    MatButtonModule
    
  ],
  entryComponents: [ActionCellRendererComponent, ActionDropdownCellRendererComponent]
})
export class UserManagementModule { }
