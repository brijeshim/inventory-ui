import { Component, OnInit } from '@angular/core';
import { gql, Apollo, WatchQueryOptions } from "apollo-angular";
import { Subscription } from "rxjs";
import { AppService } from "../app.service";


const FeatureMutationQuery = gql`mutation featureMutaion($origin: String, $data: String) {
    update_inventory_feature(where: {origin: {_eq: $origin}},_set: {feature_data: $data}) {
    affected_rows
    returning {
      origin
      feature_id
      feature_data
    }
  }
}`;

const GetFeatureQuery = gql`query MyQuery($origin: String) {
  inventory_feature(where: {origin: {_eq: $origin}}) {
    feature_data
    feature_id
    origin
  }
}`


@Component({
  selector: 'app-orgnisation-settings',
  templateUrl: './orgnisation-settings.component.html',
  styleUrls: ['./orgnisation-settings.component.scss']
})
export class OrgnisationSettingsComponent implements OnInit {

  base64Encoded: any;
  file: any;
  discountEnabled: boolean = false;
  couponEnabled: boolean = false;
  checkBoxColor = 'primary';
  userData: any;
  private querySubscription: Subscription = new Subscription();
  base64EncodedStringLogo: any;
  logoPreview: any;
  fileName: any;

  constructor(private apollo: Apollo, private appService: AppService) { }

  ngOnInit(): void {
    const attr = localStorage.getItem('user');
		const userAttr = JSON.parse(JSON.parse(JSON.stringify(attr)));
		console.log('userGroups', userAttr);
		this.userData = userAttr;
		//this.getOrganisationFeatures()
  }
  onfilesSelect(event: any) {
    const file = event.target.files[0];
    console.log(file)
    this.fileName = file['name'];
    if(file) {
      const reader = new FileReader();
      this.file = file;
      reader.onload = this._handleFileEvent.bind(this);
      reader.readAsDataURL(file);
    }
  }
  
  _handleFileEvent(readerEvt: any) {
    const binaryString = readerEvt.target.result;
    this.logoPreview = readerEvt.target.result;
    this.base64EncodedStringLogo = btoa(binaryString);
    console.log(this.base64EncodedStringLogo);
    console.log(this.logoPreview);
  }

  updateFeatures() {
    const data = {
      is_discount_enabled: this.discountEnabled,
      is_coupon_enabled: this.couponEnabled
    }
    console.log("update data",data)
    const date = (new Date).getTime();
    this.apollo.mutate({
      mutation: FeatureMutationQuery,
      variables: {
        "origin": "myshop",
        "data": JSON.stringify(data)
      }
    }).subscribe((result: any) => {
        console.log("add result", result)
        const data =  JSON.parse(result['data']["update_inventory_feature"]['returning'][0]["feature_data"]);
        this.discountEnabled = data['is_discount_enabled'];
        this.couponEnabled = data['is_coupon_enabled']
    },(error) => {
        console.log('there was an error sending the query', error);
    });
  }

  getOrganisationFeatures() {
		this.querySubscription = this.apollo.watchQuery<any>({
		  query: GetFeatureQuery,
		  variables: {
			  origin: this.userData['x-origin']
		  }
		}).valueChanges.subscribe((result: any) => {
      console.log("data",JSON.parse(result['data']['inventory_feature'][0]["feature_data"]))
      if(result['data'] && result['data']['inventory_feature']){
        const data = JSON.parse(result['data']['inventory_feature'][0]["feature_data"]);
        console.log("data",data)
        this.discountEnabled = data['is_discount_enabled'];
        this.couponEnabled = data['is_coupon_enabled']
      }
    });
  }

  setLogo() {
    const data = {
      encodedFile: this.logoPreview.replace('data:image/png;base64,',''),
      contentType: "image/png",
      fileName: this.fileName
    };
    console.log("data",data)
    this.appService.setLogo(this.userData['x-origin'],data).subscribe((result: any) => {
      console.log(result)
    },( error: any )=>{
      console.log(error);
    })
  }

  

}
