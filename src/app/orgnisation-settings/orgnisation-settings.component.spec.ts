import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrgnisationSettingsComponent } from './orgnisation-settings.component';

describe('OrgnisationSettingsComponent', () => {
  let component: OrgnisationSettingsComponent;
  let fixture: ComponentFixture<OrgnisationSettingsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrgnisationSettingsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrgnisationSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
