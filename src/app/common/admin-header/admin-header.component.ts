import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import {
  OidcClientNotification,
  OidcSecurityService,
  OpenIdConfiguration,
} from 'angular-auth-oidc-client';
import { DomSanitizer } from '@angular/platform-browser'
import { AppService } from "../../app.service";

@Component({
  selector: 'app-admin-header',
  templateUrl: './admin-header.component.html',
  styleUrls: ['./admin-header.component.scss']
})
export class AdminHeaderComponent implements OnInit, OnChanges {
  userData: any;
  show: boolean = false;
  userGroups: any;
  organisation: any;
  user: any;
  logo: any;
  constructor(public oidcSecurityService: OidcSecurityService, private appService: AppService, 
    public sanitizer: DomSanitizer) { }

  ngOnInit() {
    const attr = localStorage.getItem('user');
    const userAttr = JSON.parse(JSON.parse(JSON.stringify(attr)));
    this.userData = userAttr;
    console.log('userGroups', userAttr);
    this.user = userAttr['name'];
    if(userAttr) {
      this.organisation = this.getOrganisation()
    }
  }
  ngOnChanges() {
  }

  logout() {
    this.appService.logOff();
  }

  checkAdmin() {
    let userType: boolean = false;
    if(this.userGroups.includes('ADMIN')) {
      userType = true;
    } else {
      userType = false;
    }
    return userType;
  }

  
  getOrganisation() {
    console.log("this.userData['x-origin']", this.userData['x-origin']);
    this.appService.getOrganisationById(this.userData['x-origin']).subscribe((result: any) => {
      this.organisation = result;
      this.logo = this.sanitizer.bypassSecurityTrustResourceUrl(`data:image/png;base64, ${this.organisation['logo']['encodedFile']}`);
      console.log('this.organisation', this.organisation);
    })
  }

}
