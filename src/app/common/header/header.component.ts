import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import {
  OidcClientNotification,
  OidcSecurityService,
  OpenIdConfiguration,
} from 'angular-auth-oidc-client';
import { DomSanitizer } from '@angular/platform-browser'
import { AppService } from "../../app.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnChanges {
  userData: any;
  show: boolean = false;
  userGroups: any;
  organisation: any;
  user: any;
  logo: any;
  constructor(public oidcSecurityService: OidcSecurityService, private appService: AppService, 
    public sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.oidcSecurityService.userData$.subscribe((data: any) => {
      this.userData = data;
      console.log('userdata',this.userData)
    });
    const attr = localStorage.getItem('user');
    const userAttr = JSON.parse(JSON.parse(JSON.stringify(attr)));
    this.user = userAttr;
    console.log('userGroups', userAttr);
    if(userAttr) {
      this.organisation = this.getOrganisation()
    }
  }
  ngOnChanges() {
  }

  logout() {
    this.oidcSecurityService.logoff();
  }

  checkAdmin() {
    let userType: boolean = false;
    if(this.userGroups.includes('ADMIN')) {
      userType = true;
    } else {
      userType = false;
    }
    return userType;
  }

  getOrganisation() {
    console.log("this.user['x-origin']", this.user['x-origin']);
    this.appService.getOrganisationById(this.user['x-origin']).subscribe((result: any) => {
      this.organisation = result;
      this.logo = this.sanitizer.bypassSecurityTrustResourceUrl(`data:image/png;base64, ${this.organisation['logo']['encodedFile']}`);
      console.log('this.organisation', this.organisation);
    })
  }

}
