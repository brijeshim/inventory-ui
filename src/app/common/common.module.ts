import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatTooltipModule } from '@angular/material/tooltip';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatMenuModule} from '@angular/material/menu';
import { AuthConfigModule } from "../auth/auth-config.module";
import {MatListModule} from '@angular/material/list';
import {MatToolbarModule} from '@angular/material/toolbar';
import { CommonModule } from '@angular/common';
import { AppRoutingModule } from "../app-routing.module";
import { AdminHeaderComponent } from "./admin-header/admin-header.component";

@NgModule({
    declarations: [
    ],
    imports: [
        AppRoutingModule,
        MatIconModule,
        MatDialogModule,
        MatMenuModule,
        MatGridListModule,
        AuthConfigModule,
        MatButtonModule,
        MatListModule,
        MatToolbarModule,
        CommonModule
        
    ],
    exports: [
        CommonModule
      ]
})
export class CommonCustomModule {}