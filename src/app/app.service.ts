import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { OidcClientNotification, OidcSecurityService } from 'angular-auth-oidc-client';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class AppService {
  url: any;
  headers: HttpHeaders;
  user: any;
  token: any;
  constructor(private oidcSecurityServices: OidcSecurityService, private http: HttpClient) {
    this.url = '/'
    this.token = this.oidcSecurityServices.getAccessToken();
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: 'Bearer ' + this.token,
        'Access-Control-Allow-Origin': '*'
      }),
    };

    const attr = localStorage.getItem('user');
    const userAttr = JSON.parse(JSON.parse(JSON.stringify(attr)));
    this.user = userAttr;
    this.headers = new HttpHeaders({
      Authorization: 'Bearer ' + this.token,
      'Access-Control-Allow-Origin': '*',
      'x-origin': userAttr['x-origin']
    })
  }

  public getHeaders() {
    const attr = localStorage.getItem('user');
    const userAttr = JSON.parse(JSON.parse(JSON.stringify(attr)));
    this.user = userAttr;
    return new HttpHeaders({
      Authorization: 'Bearer ' + this.token,
      'Access-Control-Allow-Origin': '*',
      'x-origin': userAttr['x-origin']
    })
  }
  
  public encodeBase64(data: any) {
    try {
      return btoa(data);
    } catch (err) {
      return null;
      console.log(err)
    }
  }
  public decodeBase64(data: any) {
    try {
      return atob(data);
    } catch (err) {
      return null;
      console.log(err)
    }
  }
  public logOff() {
    this.oidcSecurityServices.logoff()
  }

  public getAllUsers() {
    console.log('headers',this.getHeaders())
    return this.http.get(`/v1/iam//users`,{headers:this.getHeaders()})
    .pipe(res => {
      return res;
    }, catchError((err) => {
      console.error(err);
      return throwError(err);
    }))
  }

  public getAllGroups() {
    
    return this.http.get(`/v1/iam//groups`,{headers:this.headers})
    .pipe(res => {
      return res;
    }, catchError((err) => {
      console.error(err);
      return throwError(err);
    }))
  }

  public createUser(data: any) {
    
    return this.http.post(`/v1/iam/users`,data,{headers:this.headers})
    .pipe(res => {
      return res;
    }, catchError((err) => {
      console.error(err);
      return throwError(err);
    }))
  }

  public changePassword(userid: string, password: string) {
    
    this.headers = this.headers.append('ptag', password)
    console.log('headers',this.headers)
    return this.http.put(`/v1/iam/users/${userid}/reset-password`,{},{headers:this.headers})
    .pipe(res => {
      return res;
    }, catchError((err) => {
      console.error(err);
      return throwError(err);
    }))
  }

  public assignRole(userId: string, roleId: string) {
    
    console.log('headers',this.headers)
    return this.http.put(`/v1/iam/users/${userId}/roles/${roleId}`,{},{headers:this.headers})
    .pipe(res => {
      return res;
    }, catchError((err) => {
      console.error(err);
      return throwError(err);
    }))
  }

  public setActive(userId: string, active: boolean) {
    
    console.log('headers',this.headers)
    return this.http.put(`/v1/iam/users/${userId}?enable=${active}`,{},{headers:this.headers})
    .pipe(res => {
      return res;
    }, catchError((err) => {
      console.error(err);
      return throwError(err);
    }))
  }

  public getRoles() {
    
    console.log('headers',this.headers)
    return this.http.get(`/v1/iam/roles`,{headers:this.headers})
    .pipe(res => {
      return res;
    }, catchError((err) => {
      console.error(err);
      return throwError(err);
    }))
  }

  public addBrand(data: any) {
    
    return this.http.post(`/v1/inventory/brand`,data,{headers:this.headers})
    .pipe(res => {
      return res;
    }, catchError((err) => {
      console.error(err);
      return throwError(err);
    }))
  }

  public getBrands() {
    
    console.log('headers',this.headers)
    return this.http.get(`/v1/inventory/brand`,{headers:this.headers})
    .pipe(res => {
      return res;
    }, catchError((err) => {
      console.error(err);
      return throwError(err);
    }))
  }

  public addCategory(data: any) {
    
    return this.http.post(`/v1/inventory/category`,data,{headers:this.headers})
    .pipe(res => {
      return res;
    }, catchError((err) => {
      console.error(err);
      return throwError(err);
    }))
  }

  public getCategory() {
    
    console.log('headers',this.headers)
    return this.http.get(`/v1/inventory/category`,{headers:this.headers})
    .pipe(res => {
      console.log("url",this.url)
      return res;
    }, catchError((err) => {
      console.error(err);
      return throwError(err);
    }))
  }

  public addSubCategory(data: any) {
    
    return this.http.post(`/v1/inventory/subCategory`,data,{headers:this.headers})
    .pipe(res => {
      return res;
    }, catchError((err) => {
      console.error(err);
      return throwError(err);
    }))
  }

  public getSubCategory(categoryId: string) {
    
    this.headers.append( 'x-origin', this.user['x-origin'])
    console.log('headers',this.headers)
    return this.http.get(`/v1/inventory/subCategory/category/${categoryId}`,{headers:this.headers})
    .pipe(res => {
      return res;
    }, catchError((err) => {
      console.error(err);
      return throwError(err);
    }))
  }

  public addProduct(data: any) {
    
    return this.http.post(`/v1/inventory/products`,data,{headers:this.headers})
    .pipe(res => {
      return res;
    }, catchError((err) => {
      console.error(err);
      return throwError(err);
    }))
  }

  public getAllProducts() {
    
    console.log('headers',this.headers)
    return this.http.get(`/v1/inventory/category`,{headers:this.headers})
    .pipe(res => {
      return res;
    }, catchError((err) => {
      console.error(err);
      return throwError(err);
    }))
  }

  public getAllAvailableFeatures() {
    
    console.log('headers',this.headers)
    console.log("url",this.url)
    return this.http.get(`/v1/inventory/features`,{headers:this.headers})
    .pipe(res => {
      return res;
    }, catchError((err) => {
      console.log("url",err)
      console.error(err);
      return throwError(err);
    }))
  }

  public addOrganisation(data: any) {
    this.headers = new HttpHeaders({
      Authorization: 'Bearer ' + this.oidcSecurityServices.getAccessToken(),
      'Access-Control-Allow-Origin': '*'
    })
    console.log("url",this.url)
    return this.http.post(`/v1/inventory/organization`,data,{headers:this.headers})
    .pipe(res => {
      return res;
    }, catchError((err) => {
      console.error(err);
      return throwError(err);
    }))
  }

  public getFeatures() {
    this.headers = new HttpHeaders({
      Authorization: 'Bearer ' + this.oidcSecurityServices.getAccessToken(),
      'Access-Control-Allow-Origin': '*'
    })
    console.log('headers',this.headers)
    return this.http.get(`/v1/inventory/features`,{headers:this.headers})
    .pipe(res => {
      return res;
    }, catchError((err) => {
      console.log("url",err)
      console.error(err);
      return throwError(err);
    }))
  }

  public getOrganisationById(id: any) {
    this.headers = new HttpHeaders({
      Authorization: 'Bearer ' + this.oidcSecurityServices.getAccessToken(),
      'Access-Control-Allow-Origin': '*'
    })
    console.log('headers',this.headers)
    return this.http.get(`/v1/inventory/organization/${id}`,{headers:this.headers})
    .pipe(res => {
      return res;
    }, catchError((err) => {
      console.log("url",err)
      console.error(err);
      return throwError(err);
    }))
  }

  public getAllOrganisations() {
    this.headers = new HttpHeaders({
      Authorization: 'Bearer ' + this.oidcSecurityServices.getAccessToken(),
      'Access-Control-Allow-Origin': '*'
    })
    console.log('headers',this.headers)
    return this.http.get(`/v1/inventory/organization`,{headers:this.headers})
    .pipe(res => {
      return res;
    }, catchError((err) => {
      console.log("url",err)
      console.error(err);
      return throwError(err);
    }))
  }

  public getAllFeatures() {
    this.headers = new HttpHeaders({
      Authorization: 'Bearer ' + this.oidcSecurityServices.getAccessToken(),
      'Access-Control-Allow-Origin': '*'
    })
    console.log('headers',this.headers)
    return this.http.get(`/v1/inventory/features`,{headers:this.headers})
    .pipe(res => {
      return res;
    }, catchError((err) => {
      console.log("url",err)
      console.error(err);
      return throwError(err);
    }))
  }

  public getOrgFeatures(orgId: string) {
    this.headers = new HttpHeaders({
      Authorization: 'Bearer ' + this.oidcSecurityServices.getAccessToken(),
      'Access-Control-Allow-Origin': '*'
    })
    console.log('headers',this.headers)
    return this.http.get(`/v1/inventory/organization/${orgId}/features`,{headers:this.headers})
    .pipe(res => {
      return res;
    }, catchError((err) => {
      console.log("url",err)
      console.error(err);
      return throwError(err);
    }))
  }

  public setOrgFeatures(orgId: string, data: any) {
    this.headers = new HttpHeaders({
      Authorization: 'Bearer ' + this.oidcSecurityServices.getAccessToken(),
      'Access-Control-Allow-Origin': '*'
    })
    console.log('headers',this.headers)
    return this.http.put(`/v1/inventory/organization/${orgId}/features`,data,{headers:this.headers})
    .pipe(res => {
      return res;
    }, catchError((err) => {
      console.log("url",err)
      console.error(err);
      return throwError(err);
    }))
  }

  public addFeatures(data: any) {
    this.headers = new HttpHeaders({
      Authorization: 'Bearer ' + this.oidcSecurityServices.getAccessToken(),
      'Access-Control-Allow-Origin': '*'
    })
    console.log('headers',this.headers)
    return this.http.post(`/v1/inventory/features`,data,{headers:this.headers})
    .pipe(res => {
      return res;
    }, catchError((err) => {
      console.log("url",err)
      console.error(err);
      return throwError(err);
    }))
  }

  public setLogo(origin: string, data: any) {
    this.headers = new HttpHeaders({
      Authorization: 'Bearer ' + this.oidcSecurityServices.getAccessToken(),
      'Access-Control-Allow-Origin': '*'
    })
    console.log('headers',this.headers)
    return this.http.post(`/v1/inventory/organization/${origin}/logo`,data,{headers:this.headers})
    .pipe(res => {
      return res;
    }, catchError((err) => {
      console.log("url",err)
      console.error(err);
      return throwError(err);
    }))
  }


}
