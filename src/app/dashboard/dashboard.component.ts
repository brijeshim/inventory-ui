import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { gql, Apollo, WatchQueryOptions } from "apollo-angular";
import { Observable, Subscription } from 'rxjs';
import {
  OidcClientNotification,
  OidcSecurityService,
  OpenIdConfiguration,
} from 'angular-auth-oidc-client';


export type Orders = {
  id: number;
  title: string;
  votes: number;
}
export type Query = {
  query: Orders[];
}

const  GET_POST = gql`{
  inventory_order {
    create_date
    created_by
    order_status
    payment_mode
    purchase_mode
    update_date
    updated_by
    user_id
    order_id
  }
}`;
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  
  public data: any;
  private querySubscription: any;
  userData: any;
  show: boolean = false;
  userGroups: any;
  organisation: any;
  src: any;
  @ViewChild('iframe')
  iframe!: ElementRef<any>;
  constructor(private apollo: Apollo, public oidcSecurityService: OidcSecurityService) {
   }

  ngOnInit() {
    this.src = 'http://localhost:8085';
    this.getData();
    const attr = localStorage.getItem('user');
    const userAttr = JSON.parse(JSON.parse(JSON.stringify(attr)));
    // console.log('userGroups', userAttr);
    //   if(userAttr) {
    //     this.userGroups = userAttr['user-group'].map((item: string) => {
    //       const strSplited = item.replace(/\//g,'').split('-');
    //       this.organisation = strSplited[0];
    //       return strSplited[1].toUpperCase();
    //     })
    //   }
  }

  getData() {
    // this.querySubscription = this.apollo.watchQuery<any>({query:GET_POST}).valueChanges.subscribe((result: any) => {
    //   this.data = result;
    //   console.log("data",this.data)
    // })
  }

  // checkAdmin() {
  //   let userType: boolean = false;
  //   if(this.userGroups.includes('ADMIN')) {
  //     userType = true;
  //   } else {
  //     userType = false;
  //   }
  //   return userType;
  // }
  ngAfterViewInit(): void {
    
    this.src = 'http://localhost:8085';
    this.iframe.nativeElement.src = this.src;
  }
  

}
