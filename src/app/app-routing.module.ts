import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './home/home.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import { InventoryComponent } from "./inventory/inventory.component";
import { UnauthorizedComponent } from "./unauthorized/unauthorized.component";
import { AuthorizationGuard } from "./oauth/authGuard";
import { AutoLoginAllRoutesGuard, AutoLoginPartialRoutesGuard  } from 'angular-auth-oidc-client';
import { AddProductComponent } from "./inventory/add-product/add-product.component";
import { PosUiComponent } from "./pos-ui/pos-ui.component";
import { AccountSettingsComponent } from "./account-settings/account-settings.component";
import { SuperAdminComponent } from "./super-admin/super-admin.component";
const routes: Routes = [
  { path: '', redirectTo: 'autologin', pathMatch: 'full' },
  {
    path: 'autologin',
    component: HomeComponent
  },
  {
    path: 'dashboard',
    component: DashboardComponent, canActivate: [AutoLoginPartialRoutesGuard]
  },
  {
    path: 'dashboard/user',
    component: PosUiComponent, canActivate: [AutoLoginPartialRoutesGuard]
  },
  {
    path: 'sales/sale',
    component: PosUiComponent, canActivate: [AutoLoginPartialRoutesGuard]
  },
  {
    path: 'dashboard/user/account/setting',
    component: AccountSettingsComponent, canActivate: [AutoLoginPartialRoutesGuard]
  },
  {
    path: 'dashboard/super-admin',
    component: SuperAdminComponent, loadChildren: () => import('./super-admin/super-admin.module').then(m => m.SuperAdminModule) , canActivate: [AutoLoginPartialRoutesGuard]
  },
  // {
  //   path: 'inventory',
  //   component: InventoryComponent, canActivate: [AutoLoginAllRoutesGuard]
  // },
  // {
  //   path: 'inventory/add-product',
  //   component: AddProductComponent, canActivate: [AutoLoginAllRoutesGuard]
  // },
  { path: 'forbidden', component: UnauthorizedComponent },
  { path: 'unauthorized', component: UnauthorizedComponent },
  { path: 'dashboard/admin', loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule) },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
