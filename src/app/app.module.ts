import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from "./home/home.component";
import { InventoryComponent  } from "./inventory/inventory.component";
import { HeaderComponent } from "./common/header/header.component";
import { FooterComponent } from "./common/footer/footer.component";
import { SidebarComponent } from "./common/sidebar/sidebar.component";
import { AgGridModule } from "ag-grid-angular";
import { configureAuth } from "./oauth/oAuthConfig";
import { APP_INITIALIZER } from '@angular/core';
import { UnauthorizedComponent } from './unauthorized/unauthorized.component';
import { HttpClientModule } from '@angular/common/http';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { PosUiComponent } from './pos-ui/pos-ui.component';
import { SaleComponent } from './pos-ui/sale/sale.component';
import { GraphQLModule } from "./graphql.module";
import { AuthConfigModule } from './auth/auth-config.module';

import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatSidenavModule  } from "@angular/material/sidenav";
import { MatToolbarModule } from "@angular/material/toolbar";
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import {NgxPrintModule} from 'ngx-print';
import { MatDialogModule } from '@angular/material/dialog';
import { CommonCustomModule } from "./common/common.module";
import { AdminHeaderComponent } from './common/admin-header/admin-header.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatBadgeModule } from '@angular/material/badge';
import {MatButtonModule} from '@angular/material/button';
import { AccountSettingsComponent } from './account-settings/account-settings.component';
import {MatTabsModule} from '@angular/material/tabs';
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { ActionCellRendererSalesComponent } from "./pos-ui/action-cell-renderer";
import { MatExpansionModule } from "@angular/material/expansion";
import { MatDatepickerModule } from "@angular/material/datepicker";
import {MatNativeDateModule} from '@angular/material/core';
import {MatCheckboxModule} from '@angular/material/checkbox';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FooterComponent,
    SidebarComponent,
    HeaderComponent,
    UnauthorizedComponent,
    PosUiComponent,
    SaleComponent,
    AccountSettingsComponent,
    ActionCellRendererSalesComponent,
  ],
  imports: [
    BrowserAnimationsModule, 
    NoopAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    AgGridModule.withComponents([]),
    GraphQLModule,
    NgbModule,
    GraphQLModule,
    MatSidenavModule,
    MatToolbarModule,
    AuthConfigModule,
    MatIconModule,
    NgxPrintModule,
    MatListModule,
    MatMenuModule,
    MatDialogModule,
    FormsModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    MatInputModule,
    MatSlideToggleModule,
    MatTooltipModule,
    MatBadgeModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatTabsModule,
    MatSnackBarModule,
    MatExpansionModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatCheckboxModule
  ],
  entryComponents: [ActionCellRendererSalesComponent],
  bootstrap: [AppComponent]
})
export class AppModule {}
