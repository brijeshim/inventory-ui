import { Component, OnDestroy, OnInit } from '@angular/core';
import { OidcClientNotification, OidcSecurityService, UserDataResult } from 'angular-auth-oidc-client';
import { Observable } from 'rxjs';
import { Router } from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title: any;
  isAuthenticated$: any;
  userData$: Observable<UserDataResult> = new Observable<UserDataResult>();
  constructor(public oidcSecurityService: OidcSecurityService, private route: Router) {
    this.title = 'inventory-ui';
  }
  

  ngOnInit() {
    // this.oidcSecurityService.checkAuth().subscribe(({ isAuthenticated, userData, accessToken, idToken }) => {
    //   console.log('app authenticated', isAuthenticated)
    //   this.isAuthenticated$ = isAuthenticated;
    //   if(isAuthenticated){
    //     this.route.navigate(['/dashboard']);
    //   } else {
    //     this.route.navigate(['/unauthorized']);
    //   }
    // });
    // this.userData$ = this.oidcSecurityService.userData$;
    // console.log("this.userData$",this.userData$)
  }
  logout() {
    this.oidcSecurityService.logoff();
  }
}
