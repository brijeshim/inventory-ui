import { Component, OnInit } from '@angular/core';
import { AppService } from "../app.service";
import { FormBuilder, FormControl, Validators, FormGroup } from "@angular/forms";
export function ConfirmedValidator(controlName: string, matchingControlName: string){
  return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];
      if (matchingControl.errors && !matchingControl.errors.confirmedValidator) {
          return;
      }
      if (control.value !== matchingControl.value) {
          matchingControl.setErrors({ confirmedValidator: true });
      } else {
          matchingControl.setErrors(null);
      }
  }
}
@Component({
  selector: 'app-account-settings',
  templateUrl: './account-settings.component.html',
  styleUrls: ['./account-settings.component.scss']
})
export class AccountSettingsComponent implements OnInit {
  role: any;
  form: any;
  userData: any;
  constructor(private readonly appService: AppService, private formBuilder: FormBuilder) {
    const pregex = '^[a-zA-Z0-9_@#-]{6,15}$';
    this.form = this.formBuilder.group({
      password: ['', [Validators.required, Validators.pattern(pregex)]],
      confirm_password: ['', [Validators.required, Validators.pattern(pregex)]]
    }, { 
      validator: ConfirmedValidator('password', 'confirm_password')
    })
   }

  ngOnInit(): void {
    const attr = localStorage.getItem('user');
    const userAttr = JSON.parse(JSON.parse(JSON.stringify(attr)));
    this.userData = userAttr;
    console.log('userGroups', userAttr);
      if(userAttr) {
        if(userAttr['roles'][0].indexOf('-admin') !== -1){
          this.role = 'admin';
        }else{
          this.role = 'employee'
        }
        console.log("role", this.role)
      }
  }

  get f(){
    return this.form.controls;
  }

  resetPassword() {
    this.appService.changePassword(this.userData['sub'],this.form.value['password']).subscribe((result: any) => {
      console.log("result",result.message);
      setTimeout(() => {
        
      }, 5000);
    }, (error) => {
      console.log(error)
    })
  }

}
