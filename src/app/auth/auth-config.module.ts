import { NgModule } from '@angular/core';
import { AuthModule } from 'angular-auth-oidc-client';


@NgModule({
    imports: [AuthModule.forRoot({
        config: {
            authority: 'http://localhost:8080/auth/realms/openIdAuth',
            redirectUrl: "http://localhost:4200",
            postLogoutRedirectUri: window.location.origin,
            clientId: 'inventory-auth',  
            scope: 'openid profile email',
            responseType: 'code',
            silentRenew: true,
            useRefreshToken: true,
            silentRenewUrl: window.location.origin + '/silent-renew.html',
            renewTimeBeforeTokenExpiresInSeconds: 10,
            
        }
      })],
    exports: [AuthModule],
})
export class AuthConfigModule {}
