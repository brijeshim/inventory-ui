import { Component, OnInit } from '@angular/core';
import { AppService } from "../../app.service";
@Component({
  selector: 'app-features',
  templateUrl: './features.component.html',
  styleUrls: ['./features.component.scss']
})
export class FeaturesComponent implements OnInit {
  featureData: any;
  isCreateFeature: boolean = false;
  featureName: any;
  isEnabled: boolean = false;
  isEditing: boolean = false;
  editingIndex: any;
  constructor(private appService: AppService) { }

  ngOnInit(): void {
    this.getAllFeatures();
  }

  public getAllFeatures() {
    this.appService.getAllFeatures().subscribe((result: any) => {
      this.featureData = result;
      console.log('featuredata',this.featureData)
    },(error) => {

    })
  }
  createFeature() {
    const data = {
      "enable": this.isEnabled,
      "featureName": this.featureName
    }
    this.appService.addFeatures(data).subscribe((result: any) => {
      this.getAllFeatures();
      console.log('featuredatapost',this.featureData)
    },(error) => {

    })
  }

}
