import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SuperAdminComponent } from './super-admin.component';
import { CreateOrganisationComponent } from "./create-organisation/create-organisation.component";
import { OrgDetailsComponent } from "./org-details/org-details.component";
import { OrgListComponent } from "./org-list/org-list.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { FeaturesComponent } from "./features/features.component";

const routes: Routes = [
  { 
    path: '', 
    component: DashboardComponent,
    children: [
      {
        path: '',
        pathMatch: "full",
        redirectTo: 'orgnisation/list'
      },
      {
        path: 'orgnisation/list',
        component: OrgListComponent
      },
      { path: 'orgnisation/create', component: CreateOrganisationComponent },
      { path: 'orgnisation/details/:id', component: OrgDetailsComponent },
      { path: 'orgnisation/features', component: FeaturesComponent },
    ]
},
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SuperAdminRoutingModule {}