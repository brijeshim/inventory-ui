import { Component, OnInit } from '@angular/core';
import { AppService } from "../../app.service";
import { LinkRendererComponent } from "../link-renderer";
import { GridOptions } from "ag-grid-community";
@Component({
  selector: 'app-org-list',
  templateUrl: './org-list.component.html',
  styleUrls: ['./org-list.component.scss']
})
export class OrgListComponent implements OnInit {

  orgData: any;
  frameworkComponents: any;
  gridOptions: any;
  defaultColDef: any;
  rowSelection: any;
  editType: any;
  gridApi: any;
  gridColumnApi: any;
  columnDefs: any;
  perPage: any;
  constructor(private appService: AppService) {
    this.frameworkComponents = {
		      linkRenderer: LinkRendererComponent
		}
		this.gridOptions = <GridOptions>{
			context: {
				componentParent: this
			}
		};
    this.defaultColDef = {
			sortable: true,
			resizable: true,
			filter: true,
			flex: 1,
			minWidth: 100,
      suppressDragLeaveHidesColumns: true,
			};
    	this.rowSelection = 'multiple';
		this.editType = 'fullRow';
    this.perPage = 10;
   }

  ngOnInit(): void {
    this.getAllOrganisations()
  }

  onGridReady(params: any) {
		this.gridApi = params.api;
		this.gridColumnApi = params.columnApi;
    this.columnDefs = [
			{headerName: 'S.No.', width: 80,  valueGetter: "node.rowIndex + 1", pinned: 'left' },
			{headerName: 'Organisation Origin',filter: true,pinned: 'left', field: 'originName',getQuickFilterText: function(params: any) {
				return params.value;
			}},
			{headerName: 'Organisation Name', field: 'organisationName', pinned: 'left',filter: true,getQuickFilterText: function(params: any) {
				return params.value;
			}},
			{headerName: 'Owner Email', pinned: 'left',  field: "ownerEmail", params: "images" },
			{headerName: 'Active', field: 'isActive', editable: true},
      {headerName: 'Organisation Detail', field: 'organisationId', cellRenderer: 'linkRenderer', pinned: 'right'}
		];
		this.getAllOrganisations();
	}

  public getAllOrganisations() {
    this.appService.getAllOrganisations().subscribe((result: any) => {
      this.orgData = result;
    },(error) => {

    })
  }

}
