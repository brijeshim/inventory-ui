import { Component, OnInit,AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppService } from "../../app.service";
import {FormControl} from '@angular/forms';
import { ActivatedRoute, Router } from "@angular/router";
import { MatDialog } from "@angular/material/dialog";
import { DomSanitizer } from '@angular/platform-browser'
@Component({
  selector: 'app-create-organisation',
  templateUrl: './create-organisation.component.html',
  styleUrls: ['./create-organisation.component.scss']
})
export class CreateOrganisationComponent implements OnInit,AfterViewInit {
  loading: any;
  addUserForm: any;
  submitted: boolean = false;
  groups: any;
  roles: any;
  userData: any;
  genPass: boolean = false;
  step = 0;
  toppingList: string[] = ['Extra cheese', 'Mushroom', 'Onion', 'Pepperoni', 'Sausage', 'Tomato'];
  toppings = new FormControl();
  file: any;
  fileName: any;
  availableAllFeatures: any;
  base64EncodedString: any;
  base64EncodedStringLogo: any;
  logoPreview: any;
  base64EncodedDoc1: any;
  base64EncodedDoc2: any;
  docPreview1: any;
  docPreview2: any;
  fileNameDoc1: any;
  fileNameDoc2: any;
  featureData: any[] = [
    {
      feature: 'coupon',
      value: false
    },
    {
      feature: 'discount',
      value: false
    },
    {
      feature: 'voucher',
      value: false
    }
  ]
  constructor(private readonly appService: AppService, 
    private formBuilder: FormBuilder,
    private router: Router,
    private dialog: MatDialog,
    public sanitizer: DomSanitizer) {

    const mobileRegex = '^[0-9]{10,10}$';
    const zipRegex = '^[0-9]{6,6}$';
    const group: any = {};
    this.featureData.forEach((item: any) => {
      console.log(item)
      group[item['feature']] = [item.value, Validators.required]
    });
    console.log("group",group)
    this.addUserForm = this.formBuilder.group({
      orgName: ['', Validators.required],
      orgNamefull: ['', Validators.required],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.required],
      contact: ['',[ Validators.required, Validators.pattern(mobileRegex)]],
      altContact: ['',[ Validators.required, Validators.pattern(mobileRegex)]],
      docType: ['', Validators.required],
      orgBuilding: ['', Validators.required],
      orgArea: ['', Validators.required],
      landmark: ['', Validators.required],
      district: ['', Validators.required],
      state: ['', Validators.required],
      country: ['', Validators.required],
      zip: ['', [Validators.required, Validators.pattern(zipRegex)]],
      password: ['', [Validators.required, Validators.minLength(8)]]
  });

  //
  console.log("this.addUserForm",this.addUserForm)
   }

  ngOnInit(): void {
    //this.getGroups();
    const attr = localStorage.getItem('user');
    const userAttr = JSON.parse(JSON.parse(JSON.stringify(attr)));
    this.userData = userAttr;
    this.setStep(0);
   
  }

  ngAfterViewInit() {
     this.getAllFeatures()
  }

  getGroups() {
    this.loading = true;
    const attr = localStorage.getItem('user');
    const userAttr = JSON.parse(JSON.parse(JSON.stringify(attr)));
    this.userData = userAttr;
    console.log('userGroups', userAttr);
    const org = userAttr['user-group'][0].split('-')[0].replace(/\//g,'');
    console.log("org", org)
    this.appService.getAllGroups().subscribe((result: any) => {
      this.groups = result.filter((item: any) => {
        return item.indexOf(org) !== -1;
      });
      this.loading = false;
    },( error )=>{
      console.log(error);
      this.loading = false;
    })
  }

  onSubmit() {
    this.loading = true;
    const data1 = {
      "email": this.addUserForm.controls['email'].value,
      "firstName": this.addUserForm.controls['firstName'].value,
      "lastName": this.addUserForm.controls['lastName'].value,
    }
    const data = {
      "isActive": false,
      "isKycDone": false,
      "isSubscriptionExpired": true,
      "organisationName": this.addUserForm.controls['orgNamefull'].value,
      "originName": this.addUserForm.controls['orgName'].value,
      "ownerAltContact": this.addUserForm.controls['altContact'].value,
      "ownerArea": this.addUserForm.controls['orgArea'].value,
      "ownerContact": this.addUserForm.controls['contact'].value,
      "ownerCountry": "india",
      "ownerDistrict": this.addUserForm.controls['district'].value,
      "ownerEmail": this.addUserForm.controls['email'].value,
      "ownerFirstName": this.addUserForm.controls['firstName'].value,
      "ownerLastName": this.addUserForm.controls['lastName'].value,
      "ownerState": this.addUserForm.controls['state'].value,
      "ownerStreet": this.addUserForm.controls['orgBuilding'].value,
      "ownerZipCode": this.addUserForm.controls['zip'].value,
      "ownerlandMark": this.addUserForm.controls['landmark'].value,
      "subscriptionDate": "",
      "subscriptionEndDate": ""
    }
    console.log("form data",data)
    this.submitted = true;
    this.appService.addOrganisation(data).subscribe((result: any) => {
      console.log("result",result);
      //this.addUserForm.reset();
      this.step = 0;
      this.router.navigate(['dashboard/super-admin/orgnisation/details',result['organisationId']]);
      //this.loading = false;
    },( error )=>{
      console.log(error);
      this.loading = false;
    })
  }
  get f() { return this.addUserForm.controls; }


  onfilesSelect(event: any) {
    const file = event.target.files[0];
    console.log(file)
    this.fileName = file['name'];
    if(file) {
      const reader = new FileReader();
      this.file = file;
      reader.onload = this._handleFileEvent.bind(this);
      reader.readAsDataURL(file);
    }
  }
  
  _handleFileEvent(readerEvt: any) {
    const binaryString = readerEvt.target.result;
    this.logoPreview = readerEvt.target.result;
    this.base64EncodedStringLogo = `data:image/png;base64 , ${btoa(binaryString)}`;
    console.log(this.base64EncodedStringLogo);
  }
  onfilesDoc1Select(event: any) {
    const file = event.target.files[0];
    console.log(file)
    this.fileNameDoc1 = file['name'];
    if(file) {
      const reader = new FileReader();
      this.file = file;
      reader.onload = this._handleFileDoc1Event.bind(this);
      reader.readAsDataURL(file);
    }
  }
  
  _handleFileDoc1Event(readerEvt: any) {
    const binaryString = readerEvt.target.result;
    this.docPreview1 = readerEvt.target.result;
    this.base64EncodedDoc1 = btoa(binaryString);
  }

  onfilesDoc2Select(event: any) {
    const file = event.target.files[0];
    console.log(file)
    this.fileNameDoc2 = file['name'];
    if(file) {
      const reader = new FileReader();
      this.file = file;
      reader.onload = this._handleFileDoc2Event.bind(this);
      reader.readAsDataURL(file);
    }
  }
  
  _handleFileDoc2Event(readerEvt: any) {
    const binaryString = readerEvt.target.result;
    this.docPreview2 = readerEvt.target.result;
    this.base64EncodedDoc2 =  btoa(binaryString);
  }
  createUser() {
    
    // this.appService.createUser(data).subscribe((result: any) => {
    //   this.groups = result;
    // },( error )=>{
    //   console.log(error)
    // })
  }

  getAllFeatures() {
    console.log("appservice",this.appService)
    this.loading = true;
    this.appService.getFeatures().subscribe((result: any) => {
      this.availableAllFeatures= result;
      this.loading = false;
    },( error: any )=>{
      console.log(error);
      this.loading = false;
    })
  }

  generatePassword() {
    const temp = btoa('helloworld').slice(0,8);
    this.addUserForm.controls['password'].patchValue(temp);
  }

  setStep(index: number) {
    this.step = index;
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }

  bypassSecurity(text: any) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(text)
  }

  resetInput() {
    this.addUserForm.controls['email'].patchValue('');
    this.addUserForm.controls['firstName'].patchValue('');
    this.addUserForm.controls['lastName'].patchValue('');
    this.addUserForm.controls['role'].patchValue('');
    this.addUserForm.controls['password'].patchValue('');
  }
}
