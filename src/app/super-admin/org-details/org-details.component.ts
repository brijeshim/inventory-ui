import { Component, OnInit } from '@angular/core';
import { AppService } from "../../app.service";
import { ActivatedRoute } from "@angular/router";
@Component({
  selector: 'app-org-details',
  templateUrl: './org-details.component.html',
  styleUrls: ['./org-details.component.scss']
})
export class OrgDetailsComponent implements OnInit {
  orgId: any;
  orgData: any;
  editing: boolean = true;
  editIndex: any;
  orgFeaturesData: any;
  allFeaturesData: any;
  selectedFeatures: any[] = [];
  addFeatureEnable:boolean = false;
  constructor(private appService: AppService, private acticatedRoute: ActivatedRoute) {
      this.acticatedRoute.params.subscribe((item: any) => {
        this.orgId = item.id;
      })
   }

  ngOnInit(): void {
    this.getOrgDetails();
    this.getOrgFeatures();
    this.getAllFeatures();
  }

 public getOrgDetails() {
   this.appService.getOrganisationById(this.orgId).subscribe((result: any) => {
      this.orgData = [result];
      console.log("orgdata",this.orgData)
   },(errr: any) => {
     console.log(errr)
   })
 }

 public getOrgFeatures() {
  this.appService.getOrgFeatures(this.orgId).subscribe((result: any) => {
     this.orgFeaturesData = result;
     console.log("orgfeaturedata",this.orgFeaturesData)
  },(errr: any) => {
    console.log(errr)
  })
}

public getAllFeatures() {
  this.appService.getAllFeatures().subscribe((result: any) => {
     this.allFeaturesData = result;
     console.log("orgAllfeaturedata",this.allFeaturesData)
  },(errr: any) => {
    console.log(errr)
  })
}

public setOrgFeatures(data: any) {
  this.appService.setOrgFeatures(this.orgId, data).subscribe((result: any) => {
     console.log("features updated for this organisation");
     this.getOrgFeatures()
  },(errr: any) => {
    console.log(errr)
  })
}

addFeature(event: any) {
  console.log("event",event.target.value)
  const filtered = this.allFeaturesData.filter((item: any) => {
    return item.featureId === event.target.value;
  }).map((item:any)=>{
    const data ={
      featureId: item.featureId,
      enable: true
    }
    return data;
  })
  
  console.log("filtered",filtered)


  
  this.setOrgFeatures(filtered);
  
}

}
