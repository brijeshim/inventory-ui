import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SuperAdminComponent } from './super-admin.component';
import {SuperAdminRoutingModule  } from "./super-admin-routing.module";
import { SuperAdminHeaderComponent } from "./super-admin-header/super-admin-header.component";
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatBadgeModule } from '@angular/material/badge';
import {MatButtonModule} from '@angular/material/button';
import {MatTabsModule} from '@angular/material/tabs';
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { MatExpansionModule } from "@angular/material/expansion";
import { MatDatepickerModule } from "@angular/material/datepicker";
import {MatNativeDateModule} from '@angular/material/core';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatSidenavModule  } from "@angular/material/sidenav";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatDialogModule } from '@angular/material/dialog';
import { CreateOrganisationComponent } from './create-organisation/create-organisation.component';
import { AppService } from "../app.service";
import { OrgDetailsComponent } from './org-details/org-details.component';
import { OrgListComponent } from './org-list/org-list.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AgGridModule } from "ag-grid-angular";
import { LinkRendererComponent } from "./link-renderer";
import { FeaturesComponent } from './features/features.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';
@NgModule({
  declarations: [
    SuperAdminComponent,
    SuperAdminHeaderComponent,
    CreateOrganisationComponent,
    OrgDetailsComponent,
    OrgListComponent,
    DashboardComponent,
    LinkRendererComponent,
    FeaturesComponent
  ],
  imports: [
    CommonModule,
    AgGridModule.withComponents([]),
    SuperAdminRoutingModule,
    MatListModule,
    MatMenuModule,
    MatDialogModule,
    FormsModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    MatInputModule,
    MatSlideToggleModule,
    MatTooltipModule,
    MatBadgeModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatTabsModule,
    MatSnackBarModule,
    MatExpansionModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatCheckboxModule,
    MatIconModule,
    MatSidenavModule,
    MatToolbarModule,
    PdfViewerModule

  ],
  providers: [AppService],
  entryComponents: [ LinkRendererComponent ]
})
export class SuperAdminModule { }
