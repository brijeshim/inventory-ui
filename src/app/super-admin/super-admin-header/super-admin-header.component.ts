import { Component, OnInit } from '@angular/core';
import {
  OidcClientNotification,
  OidcSecurityService,
  OpenIdConfiguration,
} from 'angular-auth-oidc-client';
import { AppService } from "../../app.service";

@Component({
  selector: 'app-super-admin-header',
  templateUrl: './super-admin-header.component.html',
  styleUrls: ['./super-admin-header.component.scss']
})
export class SuperAdminHeaderComponent implements OnInit {
  userData: any;
  show: boolean = false;
  userGroups: any;
  organisation: any;
  user: any;
  org: any;
  constructor(public oidcSecurityService: OidcSecurityService, private appService: AppService) { }

  ngOnInit() {
    const attr = localStorage.getItem('user');
    const userAttr = JSON.parse(JSON.parse(JSON.stringify(attr)));
    console.log('userGroups', userAttr);
    this.userData = userAttr;
    this.user = userAttr['name'];
    if(userAttr) {
      this.organisation = userAttr['x-origin'];
    }
  }
  ngOnChanges() {
  }

  logout() {
    this.appService.logOff();
  }

}
