import {Component} from "@angular/core";
import { ICellRendererAngularComp } from 'ag-grid-angular';

import {ICellRendererParams} from "ag-grid-community";
@Component({
    selector: 'action-cell-renderer',
    template: `
    <div style="display:felx; flex-direction: row;">
        
        <a title="Go to organisation details" [routerLink]="['/dashboard/super-admin/orgnisation/details',params.value]" style="cursor:pointer;"><mat-icon style="color: #133cafe0;">remove_red_eye</mat-icon></a>
    </div>`
 })
 export class LinkRendererComponent implements ICellRendererAngularComp {
    params: any;
    agInit(params: ICellRendererParams) {
        this.params = params;
        console.log("params",params)
    }

    refresh(params: ICellRendererParams): boolean {
        this.params = params;
        return true;
    }
    
 }