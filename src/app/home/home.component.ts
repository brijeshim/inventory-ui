import { Component, OnInit } from '@angular/core';
import {
  OidcClientNotification,
  OidcSecurityService,
  OpenIdConfiguration,
  UserDataResult
} from 'angular-auth-oidc-client';
import { Observable } from 'rxjs';
import { Router } from "@angular/router";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  userData$: Observable<UserDataResult> = new Observable<UserDataResult>();
  isAuthenticated$: any;
  userGroups: any;
  roles: any;

  constructor(public oidcSecurityService: OidcSecurityService,
  private route: Router) {}

  ngOnInit() {
  
    this.oidcSecurityService.checkAuth().subscribe(({ isAuthenticated, userData, accessToken, idToken }) => {
      //console.log('app authenticated', isAuthenticated)
      this.isAuthenticated$ = isAuthenticated;
      if(isAuthenticated){
        localStorage.setItem('user',JSON.stringify(userData));
        const role = userData['roles'].map((item: string) => {
          let temp: any; 
          if(item.indexOf('admin') !== -1 || item.indexOf('employee') !== -1) {
            temp = item
          }
          return temp? temp: '';
        }).filter((el: any) => {
          return el;
        });
        console.log('user-type',role)
        if(JSON.stringify(role[0]).indexOf('superadmin') !== -1) {
          this.route.navigate(['/dashboard/super-admin']);
        } else if(JSON.stringify(role[0]).indexOf('-admin') !== -1) {
          this.route.navigate(['/dashboard/admin']);
        } else {
          this.route.navigate(['/dashboard/user']);
        } 
        
      } else {
        this.oidcSecurityService.authorize();
      }
    });
    this.userData$ = this.oidcSecurityService.userData$;
    //console.log("this.userData$",this.userData$)
  }
  login() {
    this.oidcSecurityService.authorize();
  }

  refreshSession() {
    this.oidcSecurityService.authorize();
  }

  logout() {
    this.oidcSecurityService.logoff();
  }
}
