import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin.component';
import { AutoLoginAllRoutesGuard, AutoLoginPartialRoutesGuard  } from 'angular-auth-oidc-client';
import { OrgnisationSettingsComponent } from "../orgnisation-settings/orgnisation-settings.component";
import { DashboardComponent } from "../dashboard/dashboard.component";
const routes: Routes = [
  { 
    path: '', 
    component: AdminComponent,
    children: [
      {
        path: '',
        pathMatch: "full",
        redirectTo: 'statistics'
      },
      {
        path: 'statistics',
        component: DashboardComponent, canActivate: [AutoLoginPartialRoutesGuard]
      },
      { path: 'management', loadChildren: () => import('../user-management/user-management.module').then(m => m.UserManagementModule) },
    
      {
        path: 'inventory',
        loadChildren: () => import('../inventory/inventory.module').then((m) => m.InventoryModule)
      },
      {
        path: 'orgnisation/settings',
        component: OrgnisationSettingsComponent, canActivate: [AutoLoginPartialRoutesGuard]
      },
    ]
},
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
