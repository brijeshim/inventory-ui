import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { CommonCustomModule } from "../common/common.module";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatTooltipModule } from '@angular/material/tooltip';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatMenuModule} from '@angular/material/menu';
import { AuthConfigModule } from "../auth/auth-config.module";
import {MatListModule} from '@angular/material/list';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatToolbarModule} from '@angular/material/toolbar';
import { AdminHeaderComponent } from "../common/admin-header/admin-header.component";
import { AppService } from "../app.service";
import { UserManagementModule } from "../user-management/user-management.module";
import { OrgnisationSettingsComponent } from '../orgnisation-settings/orgnisation-settings.component';
import { AccountSettingsComponent } from './account-settings/account-settings.component';
import {MatTabsModule} from '@angular/material/tabs';
import { DashboardComponent } from "../dashboard/dashboard.component";
@NgModule({
  declarations: [
    AdminComponent,
    AdminHeaderComponent,
    OrgnisationSettingsComponent,
    AccountSettingsComponent,
    DashboardComponent
  ],
  imports: [
    AdminRoutingModule,

    CommonModule,
    MatIconModule,
    MatDialogModule,
    MatMenuModule,
    MatGridListModule,
    AuthConfigModule,
    MatButtonModule,
    MatListModule,
    MatToolbarModule,
    CommonModule,
    UserManagementModule,
    MatTabsModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    FormsModule,
    MatCheckboxModule
  ],
  providers: []
})
export class AdminModule { }
