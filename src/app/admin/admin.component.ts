import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  @ViewChild('iframe')
  iframe!: ElementRef<any>;
  src: any
  constructor() { }

  ngOnInit(): void {
    // this.src = 'http://localhost:8085';
    // this.iframe.nativeElement.src = this.src;
  }
  ngAfterViewInit(): void {
    
  }

}
