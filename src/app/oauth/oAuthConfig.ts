import { HttpClientModule } from '@angular/common/http';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { AuthModule, LogLevel, OidcConfigService } from 'angular-auth-oidc-client';
// ...

export function configureAuth(oidcConfigService: OidcConfigService) {
  // return () =>
  //   oidcConfigService.withConfig({
  //     stsServer: 'http://localhost:8080/auth/realms/inventory%20auth',
  //     redirectUrl: 'http://localhost:4200',
  //     postLogoutRedirectUri: window.location.origin,
  //     clientId: 'inventoryclient',
  //     scope: 'openid profile email',
  //     responseType: 'code',
  //     silentRenew: true,
  //     silentRenewUrl: `${window.location.origin}/silent-renew.html`,
  //     logLevel: LogLevel.Debug,
  //     renewTimeBeforeTokenExpiresInSeconds: 2000
  //   });
}