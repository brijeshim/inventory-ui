import { Component, OnInit } from '@angular/core';
import { gql, Apollo, WatchQueryOptions } from "apollo-angular";
import {
	ColDef,
	GridOptions,
  } from 'ag-grid-community';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { ActionCellRendererSalesComponent } from "./action-cell-renderer";
import { ImageCellRendererComponent } from "../inventory/image-cell-renderer";
import { SaleComponent } from "./sale/sale.component";
import { MatDialog } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators, ValidatorFn, AbstractControl, FormControl } from '@angular/forms';
import { catchError, map, tap, startWith, switchMap, debounceTime, distinctUntilChanged, takeWhile, first } from 'rxjs/operators';
import {MatSnackBar} from '@angular/material/snack-bar';
import { DomSanitizer } from '@angular/platform-browser'
const GetAllProductsQuery = gql`query MyQuery($origin: String) {
	inventory_product(where: {origin: {_eq: $origin}}) {
	  bar_code
	  base_price
	  brand_id
	  category_id
	  create_date
	  created_by
	  description
	  expiry_date
	  is_active
	  manufacturing_date
	  origin
	  product_code
	  product_name
	  product_id
	  qr_code
	  quantity
	  sellig_price
	  sub_category_id
	  updated_by
	  updated_date
	}
  }`;

@Component({
  selector: 'app-pos-ui',
  templateUrl: './pos-ui.component.html',
  styleUrls: ['./pos-ui.component.scss']
})
export class PosUiComponent implements OnInit {
	public gridApi: any;
	public gridColumnApi: any;
	public frameworkComponents: any;
	public columnDefs: any;
	public rowData: any;
	public searchText: any;
	public gridOptions: any;
	public rowSelection: any;
	public defaultColDef: any;
	public discount: any;
	public perPage = 10;
	public userData: any;
	public productsData: any;
	public dataChanged: any;
	totalAmount: any;
	totalItems: any;
	paymentType: any;
	myControl = new FormControl();
  	filteredOptions: Observable<any>;
	  country_lis= new BehaviorSubject<any>([
		{name: 'Afghanistan', code: 'AF'}, 
		{name: 'Åland Islands', code: 'AX'}, 
		{name: 'Albania', code: 'AL'}, 
		{name: 'Algeria', code: 'DZ'}, 
		{name: 'American Samoa', code: 'AS'}, 
		{name: 'AndorrA', code: 'AD'}, 
		{name: 'Angola', code: 'AO'}, 
		{name: 'Anguilla', code: 'AI'}, 
		{name: 'Antarctica', code: 'AQ'}, 
		{name: 'Antigua and Barbuda', code: 'AG'}, 
		{name: 'Argentina', code: 'AR'}, 
		{name: 'Armenia', code: 'AM'}, 
		{name: 'Aruba', code: 'AW'}, 
		{name: 'Australia', code: 'AU'}, 
		{name: 'Austria', code: 'AT'}, 
		{name: 'Azerbaijan', code: 'AZ'}, 
		{name: 'Bahamas', code: 'BS'}, 
		{name: 'Bahrain', code: 'BH'}, 
		{name: 'Bangladesh', code: 'BD'}, 
		{name: 'Barbados', code: 'BB'}, 
		{name: 'Belarus', code: 'BY'}, 
		{name: 'Belgium', code: 'BE'}, 
		{name: 'Belize', code: 'BZ'}, 
		{name: 'Benin', code: 'BJ'}, 
		{name: 'Bermuda', code: 'BM'}, 
		{name: 'Bhutan', code: 'BT'}, 
		{name: 'Bolivia', code: 'BO'}, 
		{name: 'Bosnia and Herzegovina', code: 'BA'}, 
		{name: 'Botswana', code: 'BW'}, 
		{name: 'Bouvet Island', code: 'BV'}, 
		{name: 'Brazil', code: 'BR'}, 
		{name: 'British Indian Ocean Territory', code: 'IO'}, 
		{name: 'Brunei Darussalam', code: 'BN'}, 
		{name: 'Bulgaria', code: 'BG'}, 
		{name: 'Burkina Faso', code: 'BF'}, 
		{name: 'Burundi', code: 'BI'}, 
		{name: 'Cambodia', code: 'KH'}, 
		{name: 'Cameroon', code: 'CM'}, 
		{name: 'Canada', code: 'CA'}, 
		{name: 'Cape Verde', code: 'CV'}, 
		{name: 'Cayman Islands', code: 'KY'}, 
		{name: 'Central African Republic', code: 'CF'}, 
		{name: 'Chad', code: 'TD'}, 
		{name: 'Chile', code: 'CL'}, 
		{name: 'China', code: 'CN'}, 
		{name: 'Christmas Island', code: 'CX'}, 
		{name: 'Cocos (Keeling) Islands', code: 'CC'}, 
		{name: 'Colombia', code: 'CO'}, 
		{name: 'Comoros', code: 'KM'}, 
		{name: 'Congo', code: 'CG'}, 
		{name: 'Congo, The Democratic Republic of the', code: 'CD'}, 
		{name: 'Cook Islands', code: 'CK'}, 
		{name: 'Costa Rica', code: 'CR'}, 
		{name: 'Cote D\'Ivoire', code: 'CI'}, 
		{name: 'Croatia', code: 'HR'}, 
		{name: 'Cuba', code: 'CU'}, 
		{name: 'Cyprus', code: 'CY'}, 
		{name: 'Czech Republic', code: 'CZ'}, 
		{name: 'Denmark', code: 'DK'}, 
		{name: 'Djibouti', code: 'DJ'}, 
		{name: 'Dominica', code: 'DM'}, 
		{name: 'Dominican Republic', code: 'DO'}, 
		{name: 'Ecuador', code: 'EC'}, 
		{name: 'Egypt', code: 'EG'}, 
		{name: 'El Salvador', code: 'SV'}, 
		{name: 'Equatorial Guinea', code: 'GQ'}, 
		{name: 'Eritrea', code: 'ER'}, 
		{name: 'Estonia', code: 'EE'}, 
		{name: 'Ethiopia', code: 'ET'}, 
		{name: 'Falkland Islands (Malvinas)', code: 'FK'}, 
		{name: 'Faroe Islands', code: 'FO'}, 
		{name: 'Fiji', code: 'FJ'}, 
		{name: 'Finland', code: 'FI'}, 
		{name: 'France', code: 'FR'}, 
		{name: 'French Guiana', code: 'GF'}, 
		{name: 'French Polynesia', code: 'PF'}, 
		{name: 'French Southern Territories', code: 'TF'}, 
		{name: 'Gabon', code: 'GA'}, 
		{name: 'Gambia', code: 'GM'}, 
		{name: 'Georgia', code: 'GE'}, 
		{name: 'Germany', code: 'DE'}, 
		{name: 'Ghana', code: 'GH'}, 
		{name: 'Gibraltar', code: 'GI'}, 
		{name: 'Greece', code: 'GR'}, 
		{name: 'Greenland', code: 'GL'}, 
		{name: 'Grenada', code: 'GD'}, 
		{name: 'Guadeloupe', code: 'GP'}, 
		{name: 'Guam', code: 'GU'}, 
		{name: 'Guatemala', code: 'GT'}, 
		{name: 'Guernsey', code: 'GG'}, 
		{name: 'Guinea', code: 'GN'}, 
		{name: 'Guinea-Bissau', code: 'GW'}, 
		{name: 'Guyana', code: 'GY'}, 
		{name: 'Haiti', code: 'HT'}, 
		{name: 'Heard Island and Mcdonald Islands', code: 'HM'}, 
		{name: 'Holy See (Vatican City State)', code: 'VA'}, 
		{name: 'Honduras', code: 'HN'}, 
		{name: 'Hong Kong', code: 'HK'}, 
		{name: 'Hungary', code: 'HU'}, 
		{name: 'Iceland', code: 'IS'}, 
		{name: 'India', code: 'IN'}, 
		{name: 'Indonesia', code: 'ID'}, 
		{name: 'Iran, Islamic Republic Of', code: 'IR'}, 
		{name: 'Iraq', code: 'IQ'}, 
		{name: 'Ireland', code: 'IE'}, 
		{name: 'Isle of Man', code: 'IM'}, 
		{name: 'Israel', code: 'IL'}, 
		{name: 'Italy', code: 'IT'}, 
		{name: 'Jamaica', code: 'JM'}, 
		{name: 'Japan', code: 'JP'}, 
		{name: 'Jersey', code: 'JE'}, 
		{name: 'Jordan', code: 'JO'}, 
		{name: 'Kazakhstan', code: 'KZ'}, 
		{name: 'Kenya', code: 'KE'}, 
		{name: 'Kiribati', code: 'KI'}, 
		{name: 'Korea, Democratic People\'S Republic of', code: 'KP'}, 
		{name: 'Korea, Republic of', code: 'KR'}, 
		{name: 'Kuwait', code: 'KW'}, 
		{name: 'Kyrgyzstan', code: 'KG'}, 
		{name: 'Lao People\'S Democratic Republic', code: 'LA'}, 
		{name: 'Latvia', code: 'LV'}, 
		{name: 'Lebanon', code: 'LB'}, 
		{name: 'Lesotho', code: 'LS'}, 
		{name: 'Liberia', code: 'LR'}, 
		{name: 'Libyan Arab Jamahiriya', code: 'LY'}, 
		{name: 'Liechtenstein', code: 'LI'}, 
		{name: 'Lithuania', code: 'LT'}, 
		{name: 'Luxembourg', code: 'LU'}, 
		{name: 'Macao', code: 'MO'}, 
		{name: 'Macedonia, The Former Yugoslav Republic of', code: 'MK'}, 
		{name: 'Madagascar', code: 'MG'}, 
		{name: 'Malawi', code: 'MW'}, 
		{name: 'Malaysia', code: 'MY'}, 
		{name: 'Maldives', code: 'MV'}, 
		{name: 'Mali', code: 'ML'}, 
		{name: 'Malta', code: 'MT'}, 
		{name: 'Marshall Islands', code: 'MH'}, 
		{name: 'Martinique', code: 'MQ'}, 
		{name: 'Mauritania', code: 'MR'}, 
		{name: 'Mauritius', code: 'MU'}, 
		{name: 'Mayotte', code: 'YT'}, 
		{name: 'Mexico', code: 'MX'}, 
		{name: 'Micronesia, Federated States of', code: 'FM'}, 
		{name: 'Moldova, Republic of', code: 'MD'}, 
		{name: 'Monaco', code: 'MC'}, 
		{name: 'Mongolia', code: 'MN'}, 
		{name: 'Montserrat', code: 'MS'}, 
		{name: 'Morocco', code: 'MA'}, 
		{name: 'Mozambique', code: 'MZ'}, 
		{name: 'Myanmar', code: 'MM'}, 
		{name: 'Namibia', code: 'NA'}, 
		{name: 'Nauru', code: 'NR'}, 
		{name: 'Nepal', code: 'NP'}, 
		{name: 'Netherlands', code: 'NL'}, 
		{name: 'Netherlands Antilles', code: 'AN'}, 
		{name: 'New Caledonia', code: 'NC'}, 
		{name: 'New Zealand', code: 'NZ'}, 
		{name: 'Nicaragua', code: 'NI'}, 
		{name: 'Niger', code: 'NE'}, 
		{name: 'Nigeria', code: 'NG'}, 
		{name: 'Niue', code: 'NU'}, 
		{name: 'Norfolk Island', code: 'NF'}, 
		{name: 'Northern Mariana Islands', code: 'MP'}, 
		{name: 'Norway', code: 'NO'}, 
		{name: 'Oman', code: 'OM'}, 
		{name: 'Pakistan', code: 'PK'}, 
		{name: 'Palau', code: 'PW'}, 
		{name: 'Palestinian Territory, Occupied', code: 'PS'}, 
		{name: 'Panama', code: 'PA'}, 
		{name: 'Papua New Guinea', code: 'PG'}, 
		{name: 'Paraguay', code: 'PY'}, 
		{name: 'Peru', code: 'PE'}, 
		{name: 'Philippines', code: 'PH'}, 
		{name: 'Pitcairn', code: 'PN'}, 
		{name: 'Poland', code: 'PL'}, 
		{name: 'Portugal', code: 'PT'}, 
		{name: 'Puerto Rico', code: 'PR'}, 
		{name: 'Qatar', code: 'QA'}, 
		{name: 'Reunion', code: 'RE'}, 
		{name: 'Romania', code: 'RO'}, 
		{name: 'Russian Federation', code: 'RU'}, 
		{name: 'RWANDA', code: 'RW'}, 
		{name: 'Saint Helena', code: 'SH'}, 
		{name: 'Saint Kitts and Nevis', code: 'KN'}, 
		{name: 'Saint Lucia', code: 'LC'}, 
		{name: 'Saint Pierre and Miquelon', code: 'PM'}, 
		{name: 'Saint Vincent and the Grenadines', code: 'VC'}, 
		{name: 'Samoa', code: 'WS'}, 
		{name: 'San Marino', code: 'SM'}, 
		{name: 'Sao Tome and Principe', code: 'ST'}, 
		{name: 'Saudi Arabia', code: 'SA'}, 
		{name: 'Senegal', code: 'SN'}, 
		{name: 'Serbia and Montenegro', code: 'CS'}, 
		{name: 'Seychelles', code: 'SC'}, 
		{name: 'Sierra Leone', code: 'SL'}, 
		{name: 'Singapore', code: 'SG'}, 
		{name: 'Slovakia', code: 'SK'}, 
		{name: 'Slovenia', code: 'SI'}, 
		{name: 'Solomon Islands', code: 'SB'}, 
		{name: 'Somalia', code: 'SO'}, 
		{name: 'South Africa', code: 'ZA'}, 
		{name: 'South Georgia and the South Sandwich Islands', code: 'GS'}, 
		{name: 'Spain', code: 'ES'}, 
		{name: 'Sri Lanka', code: 'LK'}, 
		{name: 'Sudan', code: 'SD'}, 
		{name: 'Suriname', code: 'SR'}, 
		{name: 'Svalbard and Jan Mayen', code: 'SJ'}, 
		{name: 'Swaziland', code: 'SZ'}, 
		{name: 'Sweden', code: 'SE'}, 
		{name: 'Switzerland', code: 'CH'}, 
		{name: 'Syrian Arab Republic', code: 'SY'}, 
		{name: 'Taiwan, Province of China', code: 'TW'}, 
		{name: 'Tajikistan', code: 'TJ'}, 
		{name: 'Tanzania, United Republic of', code: 'TZ'}, 
		{name: 'Thailand', code: 'TH'}, 
		{name: 'Timor-Leste', code: 'TL'}, 
		{name: 'Togo', code: 'TG'}, 
		{name: 'Tokelau', code: 'TK'}, 
		{name: 'Tonga', code: 'TO'}, 
		{name: 'Trinidad and Tobago', code: 'TT'}, 
		{name: 'Tunisia', code: 'TN'}, 
		{name: 'Turkey', code: 'TR'}, 
		{name: 'Turkmenistan', code: 'TM'}, 
		{name: 'Turks and Caicos Islands', code: 'TC'}, 
		{name: 'Tuvalu', code: 'TV'}, 
		{name: 'Uganda', code: 'UG'}, 
		{name: 'Ukraine', code: 'UA'}, 
		{name: 'United Arab Emirates', code: 'AE'}, 
		{name: 'United Kingdom', code: 'GB'}, 
		{name: 'United States', code: 'US'}, 
		{name: 'United States Minor Outlying Islands', code: 'UM'}, 
		{name: 'Uruguay', code: 'UY'}, 
		{name: 'Uzbekistan', code: 'UZ'}, 
		{name: 'Vanuatu', code: 'VU'}, 
		{name: 'Venezuela', code: 'VE'}, 
		{name: 'Viet Nam', code: 'VN'}, 
		{name: 'Virgin Islands, British', code: 'VG'}, 
		{name: 'Virgin Islands, U.S.', code: 'VI'}, 
		{name: 'Wallis and Futuna', code: 'WF'}, 
		{name: 'Western Sahara', code: 'EH'}, 
		{name: 'Yemen', code: 'YE'}, 
		{name: 'Zambia', code: 'ZM'}, 
		{name: 'Zimbabwe', code: 'ZW'} 
	  ]);
	private querySubscription: Subscription = new Subscription();
  constructor(private readonly _dialog: MatDialog,
	private _snackBar: MatSnackBar,
	private sanitizer: DomSanitizer,
	private apollo: Apollo) {
    this.frameworkComponents = {
			multiImageRendered: ImageCellRendererComponent,
			actionCellRenderer: ActionCellRendererSalesComponent
		}
		this.defaultColDef = {
			sortable: true,
			resizable: true,
			filter: true,
			flex: 1,
			minWidth: 100,
      suppressDragLeaveHidesColumns: true,
			};
	this.gridOptions = <GridOptions>{
		deltaRowDataMode: true,
		context: this,
		getRowNodeId: (row: any) => {console.log("row['product_id']",row['product_id']);  return row['product_id'];},
		};
    	this.rowSelection = 'multiple';
		const a =[{id:"hjksh"},{id: 'jk'}]
		this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(null),
      debounceTime(500),
      distinctUntilChanged(),
      switchMap(val => this.country_lis)
      )
   }

  ngOnInit(): void {
	const attr = localStorage.getItem('user');
	const userAttr = JSON.parse(JSON.parse(JSON.stringify(attr)));
	console.log('userGroups', userAttr);
	this.userData = userAttr;
	this.getAllProductsApollo()
  }
  onGridReady(params: any) {
		this.gridApi = params.api;
		this.gridColumnApi = params.columnApi;
		this.columnDefs = [
			{headerName: 'S.No.', width: 80,  valueGetter: "node.rowIndex + 1", pinned: "left" },
			{headerName: 'Product Id', width: 150, field: 'product_id', pinned: "left",filter: true,getQuickFilterText: function(params: any) {
				return params.value;
			}},
			// {headerName: 'Image',  field: "images", cellRenderer: 'multiImageRendered', params: "images", editable: true },
			{headerName: 'Product Name', width: 150, pinned: "left",filter: true, field: 'product_name',getQuickFilterText: function(params: any) {
				return params.value;
			}},
			{headerName: 'Price', field: 'sellig_price'},
			{headerName: 'Quantity', field: 'quantity', pinned: "right", editable: true},
			// {headerName: 'Desc', field: 'desc'},
			// ,
			// {headerName: 'Specifications', field: 'specification'},
			,
			{headerName: 'Action', width: 80, cellRenderer: 'actionCellRenderer', pinned: 'right'}
		];
		this.rowData= [];
		this.gridApi.sizeColumnsToFit();
		// this.rowData = [
		// 	{ product_id:1, make: 'Toyota', model: 'Celica', price: 35000, qty: 10, images: ['/assets/images/product.png'], desc: "", specification: [] },
		// 	{ product_id:2,make: 'Ford', model: 'Mondeo', price: 32000, qty: 10, images: ['/assets/images/product.png'], desc: "", specification: []  },
		// 	{ product_id:3,make: 'Porsche', model: 'Boxter', price: 72000, qty: 10,images: ['/assets/images/product.png'], desc: "", specification: []  },
		// 	{ product_id:1, make: 'Toyota', model: 'Celica', price: 35000, qty: 10, images: ['/assets/images/product.png'], desc: "", specification: []  },
		// 	{ product_id:2,make: 'Ford', model: 'Mondeo', price: 32000, qty: 10, images: ['/assets/images/product.png'], desc: "", specification: []  },
		// 	{ product_id:3,make: 'Porsche', model: 'Boxter', price: 72000, qty: 10,images: ['/assets/images/product.png'], desc: "", specification: []  },
		// 	{ product_id:1, make: 'Toyota', model: 'Celica', price: 35000, qty: 10, images: ['/assets/images/product.png'], desc: "", specification: []  },
		// 	{ product_id:2,make: 'Ford', model: 'Mondeo', price: 32000, qty: 10, images: ['/assets/images/product.png'], desc: "", specification: []  },
		// 	{ product_id:3,make: 'Porsche', model: 'Boxter', price: 72000, qty: 10,images: ['/assets/images/product.png'], desc: "", specification: []  },
		// 	{ product_id:1, make: 'Toyota', model: 'Celica', price: 35000, qty: 10, images: ['/assets/images/product.png'], desc: "", specification: []  },
		// 	{ product_id:2,make: 'Ford', model: 'Mondeo', price: 32000, qty: 10, images: ['/assets/images/product.png'], desc: "", specification: []  },
		// 	{ product_id:3,make: 'Porsche', model: 'Boxter', price: 72000, qty: 10,images: ['/assets/images/product.png'], desc: "", specification: []  }
		// ];

	}
  
  addProduct() {
    const data ={ product_id:1, make: 'Toyota', model: 'Celica', price: 35000, qty: 10, images: ['/assets/images/product.png'], desc: "", specification: [] };
    // this.rowData = [...this.rowData, data];
    // this.gridApi.setData(this.rowData)
    this.gridApi.applyTransaction({
      add: [data],
      index: this.rowData.length -1 
    });
	this.gridApi.sizeColumnsToFit();
  }

  setItem(event: any,ele: HTMLInputElement) {
	const productData = this.productsData.filter((item: any) => {
		return item.product_id === event.option.value;
	})[0];
	const data = this.gridApi.getRenderedNodes();
	const alreadyPresent = data.filter((rowNode: any) => {
		return rowNode.data['product_id'] === event.option.value 
	});
	if(alreadyPresent.length > 0) {
		if(alreadyPresent[0].data) {
			alreadyPresent[0].data['quantity'] = alreadyPresent[0].data['quantity'] + 1;
		}
		console.log("alreadyPresent",alreadyPresent)
		this.gridApi.applyTransaction({
			update: [alreadyPresent[0].data]
		  });
	} else {
		if(productData) {
			productData['quantity'] = 1;
		}
		console.log(event,productData, this.productsData)
		// this.rowData = [...this.rowData, data];
		// this.gridApi.setData(this.rowData)
		this.gridApi.applyTransaction({
		  add: [productData]
		});
		
	}
	ele.value = '';
	ele.blur();
	this.totalAmountToBePaid()
	this.gridApi.sizeColumnsToFit();
	console.log(event);

  }
  
  openInfoDialog() {
    const data = {
      data: this.rowData,
      discount: this.discount
    };

    this._dialog.open(SaleComponent, { disableClose: true , data });
  }

  onFilterTextBoxChanged(){}
  getRowHeight() {
	  return 45;
  }

  getAllProductsApollo() {
	this.querySubscription = this.apollo.watchQuery<any>({
	  query: GetAllProductsQuery,
	  variables: {
		  origin: this.userData['x-origin']
	  },
	  pollInterval: 5000
	}).valueChanges.subscribe((result: any) => {
		this.productsData = result['data']['inventory_product'].map((item: any) => {
			return {
				"base_price": item.base_price,
				"brand_id": item.brand_id,
				"category_id": item.category_id,
				"create_date": item.create_date,
				"created_by": item.created_by,
				"description": item.description,
				"expiry_date": item.expiry_date,
				"is_active":item.is_active,
				"manufacturing_date": item.manufacturing_date,
				"origin": item.origin,
				"product_code": item.product_code,
				"product_name": item.product_name,
				"product_id": item.product_id,
				"quantity": item.quantity,
				"sellig_price": item.sellig_price,
				"sub_category_id":item.sub_category_id,
				"updated_by": item.updated_by,
				"updated_date": item.updated_date
			}
		});
	},(error: any) => {
		console.log('there was an error sending the query', error);
	});
	}

	public totalNumerofItems() {
		const data = this.gridApi.getRenderedNodes()
		this.totalItems = data && data.length > 0 ? data.length: 0;
	}

	public totalAmountToBePaid() {
		const data = this.gridApi.getRenderedNodes()
		let sum = 0;
		let qty = 0;
		if(data.length> 0) {
			data.forEach((rowNode: any) => {
				
				console.log("forEachNode",rowNode)
				sum = sum + rowNode.data["sellig_price"] * rowNode.data["quantity"];
				qty = qty + 1 * rowNode.data["quantity"];

			});
		}
		this.dataChanged = false;
		this.totalItems = qty;
		this.totalAmount = sum;
	}

	nextOrder() {
		let tabledData = <any>[];
		const data = this.gridApi.getRenderedNodes()
		data.forEach((rowNode: any) => {
			tabledData.push(rowNode.data)
		});
		if(tabledData.length > 0) {
			this.gridApi.applyTransaction({
				remove: tabledData
			});
		}

		this.totalAmountToBePaid();
	}

	onRowDataChanged(event : any) {
		this.dataChanged = true;
		console.log('this.gridApi.getRenderedNodes()',this.gridApi.getRenderedNodes())
	}

}
