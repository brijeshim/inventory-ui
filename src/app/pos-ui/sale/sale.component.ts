import { ChangeDetectionStrategy, Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-sale',
  templateUrl: './sale.component.html',
  styleUrls: ['./sale.component.scss']
})
export class SaleComponent implements OnInit {
  subTotal: any;
  totalBill: any;
  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
    console.log(this.data);
    this.calculateBill();
   }

  ngOnInit(): void {
    
  }
  calculateBill() {
    let total = 0;
    this.data.data.forEach((element: any) => {
      total = parseInt(element.price) + total
    })
    this.subTotal = total;
    this.totalBill = this.subTotal - (this.data.discount) ? 0 : this.data.discount
  }

  onPrint() {
    window.print()
  }


}
